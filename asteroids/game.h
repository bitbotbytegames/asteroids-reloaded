/// <summary>
/// @author			Alan Bolger		|	Gary Kelly
/// @student_id		C00232036		|	C00207281
/// @time_taken		?????
/// @est_time		> 80 hours
/// @date			06.03.2018
/// </summary>

#ifndef GAME
#define GAME

#include "Orb.h"
#include "Asteroid.h"
#include "EnemyShip.h"
#include "Explosion.h"
#include "MyVector2D.h"
#include "PlayerShip.h"
#include "Globals.h"
#include <SFML/Graphics.hpp>

class Game
{
public:
	Game();
	~Game();
	/// <summary>
	/// Main method for game
	/// </summary>
	void run();

	// Game state enums
	enum GameState
	{
		SPLASH_SCREEN,
		MAIN_MENU,
		MAP_SCREEN,
		HANGAR_SCREEN,
		MARKET_SCREEN,
		HELP_SCREEN,
		PLAY_GAME
	};

	// Level enums
	enum Level
	{
		MERCURY,
		VENUS,
		EARTH,
		MOON,
		JUPITER,
		SATURN,
		URANUS,
		NEPTUNE
	};

private:
	void processEvents();
	void update(sf::Time t_deltaTime);
	void render();	
	void setupFontAndText();
	void loadTexturesAndSound();
	void gameControls(); // Game play input
	void gameStateManager(GameState t_gameState); // Looks after user input for the menu screens
	void drawScreen(GameState t_gameState); // Draws the chosen image to the screen
	void gamePlayMode(); // This is the actual game						 
	void collisionDetection(); // Detect collisions between player and asteroids & enemies
	double getDistanceBetweenTwoPoints(MyVector2D t_vectorOne, MyVector2D t_vectorTwo); // Get distance between two points																						
	void generateOrb(MyVector2D t_position); // Magical random orb generator
	void setupAsteroids(); // Setup asteroids
	void showUI(); // Game ui				   
	void checkPlanetUnlocks(); // Check if enough orbs have been collected for planet unlocks							   
	void checkIfPlayerIsAlive(); // Check if player is alive								
	void resetEnemiesAndExplosions();  // Reset enemies

	sf::Time delayTime; // Time between player bullet shots
	Asteroid m_asteroids[MAX_ASTEROIDS]; // Array to hold asteroids
	Asteroid m_asteroidsMedium[MAX_MEDIUM_ASTEROIDS]; // Array for medium asteroids
	Asteroid m_asteroidsSmall[MAX_SMALL_ASTEROIDS]; // Array for small asteroids
	int m_asteroidsMediumArrayPos; // Current medium asteroids array position
	int m_asteroidsSmallArrayPos; // Current small asteroids array position
	bool m_gamePaused; // True if game is paused
	bool m_gameOver; // True if player has died

	Orb m_orbs[MAX_ORBS]; // Array to hold orbs
	Explosion m_explosions[MAX_EXPLOSIONS]; // Array to hold explosions
	PlayerShip m_playerShip; // The player's ship, I kid you not

	// Enemy ships - didn't bother with an array, just respawn either enemy if they're dead
	EnemyShip m_enemyShip_1; // Enemy ship 1
	EnemyShip m_enemyShip_2; // Enemy ship 2

	GameState m_gameState; // Stores current game state
	MyVector2D m_mousePosition; // Stores position of mouse pointer when mouse is clicked

	sf::RenderWindow m_window; // Window
	sf::Font m_normalFont; // Font
	sf::Text m_displayText; // Displays selected planet on map screen and upgrade cost on market screen
	int m_explosionArrayPos; // Keeps track of the next explosion in the array
	int m_orbArrayPos; // Current orbs array position

	sf::Text m_yellowOrbsReqText;
	sf::Text m_purpleOrbsReqText;
	sf::Text m_greenOrbsReqText;
	int m_yellowOrbsReq;
	int m_purpleOrbsReq;
	int m_greenOrbsReq;

	bool m_keyNotPressed; // For checking if a key was pressed
	bool m_bulletFired; // This bool knows if you fired a bullet
	double m_distance; // Used for collision detection
	int m_randomNumber;

	// Menu background stuff
	sf::Texture m_splashScreenTexture;
	sf::Sprite m_splashScreenSprite;
	sf::Texture m_mainMenuTexture;
	sf::Sprite m_mainMenuSprite;
	sf::Texture m_mapScreenTexture;
	sf::Sprite m_mapScreenSprite;
	sf::Texture m_hangarScreenTexture;
	sf::Sprite m_hangarScreenSprite;
	sf::Texture m_marketScreenTexture;
	sf::Sprite m_marketScreenSprite;
	sf::Texture m_helpScreenTexture;
	sf::Sprite m_helpScreenSprite;
	sf::Texture m_venusLockedTex;
	sf::Sprite m_venusLockedSprite;
	sf::Texture m_earthLockedTex;
	sf::Sprite m_earthLockedSprite;
	sf::Texture m_moonLockedTex;
	sf::Sprite m_moonLockedSprite;
	sf::Texture m_marsLockedTex;
	sf::Sprite m_marsLockedSprite;
	sf::Texture m_jupiterLockedTex;
	sf::Sprite m_jupiterLockedSprite;
	sf::Texture m_saturnLockedTex;
	sf::Sprite m_saturnLockedSprite;
	sf::Texture m_uranusLockedTex;
	sf::Sprite m_uranusLockedSprite;
	sf::Texture m_neptuneLockedTex;
	sf::Sprite m_neptuneLockedSprite;
	sf::Texture m_pauseScreenTex;
	sf::Sprite m_pauseScreenSprite;
	sf::Texture m_gameOverScreenTex;
	sf::Sprite m_gameOverScreenSprite;

	// Game ui stuff
	sf::Texture m_livesUITexture;
	sf::Sprite m_livesUISprite;
	sf::Texture m_orbsUITexture;
	sf::Sprite m_orbsUISprite;
	sf::Text m_livesUIText;
	sf::Text m_yelOrbsUIText;
	sf::Text m_redOrbsUIText;
	sf::Text m_purpOrbsUIText;
	sf::Text m_greenOrbsUIText;
	sf::Text m_scrapOrbsUIText;

	// Upgrade related stuff
	Inventory m_inventory; // Inventory object, stores orbs
	int m_currentCash; // Stores all cash collected by the player
	int m_upgradeCost; // Stores upgrade cost
	sf::RectangleShape m_laserUpgradeBar;
	sf::RectangleShape m_holdUpgradeBar;
	sf::RectangleShape m_engineUpgradeBar;
	sf::RectangleShape m_reactorUpgradeBar;
	int m_laserUpgradeBarLength;
	int m_holdUpgradeBarLength;
	int m_engineUpgradeBarLength;
	int m_reactorUpgradeBarLength;
	int m_laserUpgradeLevel;
	int m_holdUpgradeLevel;
	int m_engineUpgradeLevel;
	int m_reactorUpgradeLevel;
	sf::Text m_upgradeCostText;
	sf::Text m_yellowOrbsText;
	sf::Text m_redOrbsText;
	sf::Text m_purpleOrbsText;
	sf::Text m_greenOrbsText;
	sf::Text m_scrapOrbsText;

	// Unlock bools
	bool m_isVenusUnlocked;
	bool m_isEarthUnlocked;
	bool m_isMoonUnlocked;
	bool m_isMarsUnlocked;
	bool m_isJupiterUnlocked;
	bool m_isSaturnUnlocked;
	bool m_isUranusUnlocked;
	bool m_isNeptuneUnlocked;

	// Sound stuff
	sf::SoundBuffer m_asteroidBreakBuf;
	sf::SoundBuffer m_explosionBuf;
	sf::SoundBuffer m_uiBackBuf;
	sf::SoundBuffer m_uiSelectBuf;
	sf::SoundBuffer m_uiErrorBuf;
	sf::SoundBuffer m_uiSuccessBuf;
	sf::Sound m_asteroidBreakSound;
	sf::Sound m_explosionSound;
	sf::Sound m_uiBackSound;
	sf::Sound m_uiSelectSound;
	sf::Sound m_uiErrorSound;
	sf::Sound m_uiSuccessSound;

	bool m_exitGame; // Exit game bool
};

#endif // !GAME


#include "Orb.h"

// Default constructor
Orb::Orb()
{
	loadTexturesAndSound();
	m_alive = false;
	m_orbType = std::rand() % 5; // Randomise orb type
	m_sprite.setOrigin(10, 10);
	setOrb(); // Set orb type / colour
}

// Load textures
void Orb::loadTexturesAndSound()
{
	m_textureYellow.loadFromFile("ASSETS\\SPRITES\\yellow_orb.png");
	m_textureRed.loadFromFile("ASSETS\\SPRITES\\red_orb.png");
	m_texturePurple.loadFromFile("ASSETS\\SPRITES\\purple_orb.png");
	m_textureGreen.loadFromFile("ASSETS\\SPRITES\\green_orb.png");
	m_textureScrap.loadFromFile("ASSETS\\SPRITES\\scrap_orb.png");

	m_orbPickupBuf.loadFromFile("ASSETS\\AUDIO\\orb_pickup.wav");
	m_orbPickupSound.setBuffer(m_orbPickupBuf);
	m_orbPickupSound.setVolume(40.0f);
}

// Set orb type / colour
void Orb::setOrb()
{
	switch (m_orbType)
	{
	case YELLOW:
		m_sprite.setTexture(m_textureYellow);
		break;

	case RED:
		m_sprite.setTexture(m_textureRed);
		break;

	case PURPLE:
		m_sprite.setTexture(m_texturePurple);
		break;

	case GREEN:
		m_sprite.setTexture(m_textureGreen);
		break;

	case SCRAP:
		m_sprite.setTexture(m_textureScrap);
		break;
	}
}

// Return orb type
int Orb::getOrbType()
{
	return m_orbType;
}

// Check if orb is alive
bool Orb::isAlive()
{
	return m_alive;
}

// Kill orb
void Orb::killOrb()
{
	m_alive = false;
}

// Collision detection (pickup)
void Orb::collectOrb(MyVector2D t_playerShipPosition, Inventory &t_object, int t_holdUpgradeLevel)
{
	// Radius of orb is 10 + radius of player ship is 32 = 42
	if (getDistanceBetweenTwoPoints(t_playerShipPosition, m_position) < 42)
	{
		m_orbPickupSound.play();

		switch (m_orbType)
		{
		case YELLOW:
			t_object.addOrbs(orbType::YELLOW, 1, t_holdUpgradeLevel);
			killOrb();
			break;

		case RED:
			t_object.addOrbs(orbType::RED, 1, t_holdUpgradeLevel);
			killOrb();
			break;

		case PURPLE:
			t_object.addOrbs(orbType::PURPLE, 1, t_holdUpgradeLevel);
			killOrb();
			break;

		case GREEN:
			t_object.addOrbs(orbType::GREEN, 1, t_holdUpgradeLevel);
			killOrb();
			break;

		case SCRAP:
			t_object.addOrbs(orbType::SCRAP, 1, t_holdUpgradeLevel);
			killOrb();
			break;
		}
	}
}

// Get distance between two points
double Orb::getDistanceBetweenTwoPoints(MyVector2D t_vectorOne, MyVector2D t_vectorTwo)
{
	double f_distance = std::sqrt(((t_vectorOne.x - t_vectorTwo.x) * (t_vectorOne.x - t_vectorTwo.x))
		+ ((t_vectorOne.y - t_vectorTwo.y) * (t_vectorOne.y - t_vectorTwo.y)));
	return f_distance;
}

// Activate orb
void Orb::activate(MyVector2D t_position)
{
	m_position = t_position;
	m_sprite.setPosition(m_position);
	m_alive = true;
}

// Get sprite
sf::Sprite Orb::getSprite()
{
	return m_sprite;
}
#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Globals.h"

class Inventory
{
private:
	int m_yellowOrb;
	int m_redOrb;
	int m_purpleOrb;
	int m_greenOrb;
	int m_scrapOrb;

	// Total orbs collected
	int m_totalYellowOrbs;
	int m_totalRedOrbs;
	int m_totalPurpleOrbs;
	int m_totalGreenOrbs;
	int m_totalScrapOrbs;

public:
	Inventory(); // Default constructor
	void addOrbs(int t_orbType, int t_amount, int t_upgradeLevel); // Add orbs
	void decreaseOrbs(int t_orbType, int t_amount); // Decrease orbs
	int getYellowOrbs();
	int getRedOrbs();
	int getPurpleOrbs();
	int getGreenOrbs();
	int getScrapOrbs();
	int getTotalYellowOrbs();
	int getTotalRedOrbs();
	int getTotalPurpleOrbs();
	int getTotalGreenOrbs();
	int getTotalScrapOrbs();	
	void holdCapacity(int t_holdCapacityLevel); // Checks if maximum hold capacity has been reached
};
#include "Asteroid.h"
#include "PlayerShip.h"
#define TWO_DEGREES 0.0349066

#include <iostream>

// Default constructor
Asteroid::Asteroid()
{
	loadTexturesAndSound();
	m_alive = false; // Check if asteroid is alive	
	m_speed = 0.5f;
	setAsteroid();
}

// Set starting point for asteroid
void Asteroid::setAsteroid()
{
	m_randomNumber = std::rand() % 4 + 1; // Random number between 1 and 4

	switch (m_randomNumber)
	{
	case 1: // Spawn from left
		m_position = MyVector2D(-100, std::rand() % 600);
		m_direction = MyVector2D(950, std::rand() % 600) - m_position;
		break;

	case 2: // Spawn from right
		m_position = MyVector2D(900, std::rand() % 600);
		m_direction = MyVector2D(-100, std::rand() % 600) - m_position;
		break;

	case 3: // Spawn from top
		m_position = MyVector2D(std::rand() % 800, -100);
		m_direction = MyVector2D(std::rand() % 800, 700) - m_position;
		break;

	case 4: // Spawn from bottom
		m_position = MyVector2D(std::rand() % 800, 700);
		m_direction = MyVector2D(std::rand() % 800, -100) - m_position;
		break;
	}

	m_direction.normalise();
}

// Set starting point for asteroid with a position
void Asteroid::setAsteroid(MyVector2D t_position, MyVector2D t_direction)
{
	m_position = t_position;
	m_direction = t_direction;
	m_sprite.setRotation(0);
}

// Update
void Asteroid::update(MyVector2D t_playerPosition)
{
	if (m_alive)
	{
		m_position += m_direction * m_speed;
		m_sprite.setPosition(m_position);
		m_sprite.rotate(1);
		boundryCheck();
	}
}

// Set asteroid as small, medium or large
void Asteroid::asteroidSize(enum asteroidType t_asteroidSize)
{
	switch (t_asteroidSize)
	{
	case asteroidType::SMALL:
		m_sprite.setTexture(m_smallTexture);
		m_sprite.setOrigin(16, 16);
		m_radius = 16;
		break;
		
	case asteroidType::MEDIUM:
		m_sprite.setTexture(m_mediumTexture);
		m_sprite.setOrigin(32, 32);
		m_radius = 32;
		break;

	case asteroidType::LARGE:
		m_sprite.setTexture(m_largeTexture);
		m_sprite.setOrigin(48, 48);
		m_radius = 48;
		break;
	}
}

// Respawn asteroid if it goes off the screen
void Asteroid::boundryCheck()
{
	if (m_position.x < -150 || m_position.x > 950 || m_position.y < -150 || m_position.y > 750)
	{
		setAsteroid();
	}
}

// Get sprite
sf::Sprite Asteroid::getSprite()
{
	return m_sprite;
}

// Is the asteroid alive?
bool Asteroid::isAlive()
{
	return m_alive;
}

// Kill asteroid
void Asteroid::kill()
{
	m_alive = false;
}

// Get position
MyVector2D Asteroid::getPosition()
{
	return m_position;
}

// Get radius of asteroid
double Asteroid::getRadius()
{
	return m_radius;
}

MyVector2D Asteroid::getDirection()
{
	return m_direction;
}

MyVector2D Asteroid::getOppositeDirection()
{
	return -m_direction;
}

void Asteroid::activate()
{
	m_alive = true;
}

// Load textures and sound for asteroids
void Asteroid::loadTexturesAndSound()
{
	m_smallTexture.loadFromFile("ASSETS\\SPRITES\\small_asteroid.png");
	m_mediumTexture.loadFromFile("ASSETS\\SPRITES\\medium_asteroid.png");
	m_largeTexture.loadFromFile("ASSETS\\SPRITES\\large_asteroid.png");
}
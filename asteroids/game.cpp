/// <summary>
/// @author			Alan Bolger		|	Gary Kelly
/// @student_id		C00232036		|	C00207281
/// @time_taken		Approx 70 hours in total
/// @est_time		> 80 hours
/// @date			06.03.2018
/// </summary>

// Use debug settings in Globals.h for unlimited cash, orbs, etc...

#include "Game.h"
#include <iostream>

Game::Game() :
	m_window{ sf::VideoMode{ 800, 600, 32 }, "Asteroids Reloaded" },
	m_exitGame{ false },
	m_gameState{ SPLASH_SCREEN },
	m_keyNotPressed{ true },
	m_gamePaused{ false },
	m_gameOver{ false },
	m_bulletFired{ false },
	m_currentCash{ STARTING_CASH },
	m_upgradeCost{ 0 },
	m_explosionArrayPos{ 0 },
	m_asteroidsMediumArrayPos{ 0 },
	m_asteroidsSmallArrayPos{ 0 },
	m_orbArrayPos{ 0 },
	m_yellowOrbsReq{ 0 },
	m_purpleOrbsReq{ 0 },
	m_greenOrbsReq{ 0 },
	m_isVenusUnlocked{ false },
	m_isEarthUnlocked{ false },
	m_isMoonUnlocked{ false },
	m_isMarsUnlocked{ false },	
	m_isJupiterUnlocked{ false },
	m_isSaturnUnlocked{ false },
	m_isUranusUnlocked{ false },
	m_isNeptuneUnlocked{ false },
	m_laserUpgradeLevel{ 1 }, // Upgrades make laser shoot faster
	m_holdUpgradeLevel{ 1 },
	m_engineUpgradeLevel{ 1 }, // Upgrades make thrust speed better
	m_reactorUpgradeLevel{ 1 } // Increases starting lives
{
	// Game setup
	setupFontAndText();
	loadTexturesAndSound();
	setupAsteroids();
	delayTime = LASER_LVL_1; // Delay between bullets, used when upgrading laser

	m_laserUpgradeBarLength = UPGRADE_BAR_INCREMENT;
	m_holdUpgradeBarLength = UPGRADE_BAR_INCREMENT;
	m_engineUpgradeBarLength = UPGRADE_BAR_INCREMENT;
	m_reactorUpgradeBarLength = UPGRADE_BAR_INCREMENT;

	m_laserUpgradeBar.setFillColor(sf::Color::Red);
	m_laserUpgradeBar.setPosition(280, 230);
	m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));

	m_holdUpgradeBar.setFillColor(sf::Color::Red);
	m_holdUpgradeBar.setPosition(280, 259);
	m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));

	m_engineUpgradeBar.setFillColor(sf::Color::Red);
	m_engineUpgradeBar.setPosition(280, 288);
	m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));

	m_reactorUpgradeBar.setFillColor(sf::Color::Red);
	m_reactorUpgradeBar.setPosition(280, 317);
	m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));

	m_yellowOrbsReqText.setString("00");
	m_purpleOrbsReqText.setString("00");
	m_greenOrbsReqText.setString("00");
}

Game::~Game()
{
}

void Game::run()
{
	sf::Clock clock;
	sf::Clock otherClock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	sf::Time timePerFrame = sf::seconds(1.f / 60.f); // 60 fps
	sf::Time bulletTimer = sf::Time::Zero;

	while (m_window.isOpen())
	{
		processEvents(); // As many as possible
		timeSinceLastUpdate += clock.restart();
		bulletTimer += otherClock.restart();

		if (bulletTimer > delayTime)
		{
			m_bulletFired = false;
			bulletTimer = sf::Time::Zero;
		}

		while (timeSinceLastUpdate > timePerFrame)
		{
			timeSinceLastUpdate -= timePerFrame;
			processEvents(); // At least 60 fps
			update(timePerFrame); // 60 fps
		}

		render(); // As many as possible
	}
}

/// <summary>
/// Handle user and system events / input
/// Get key presses / mouse moves etc. from OS
/// and user
/// </summary>
void Game::processEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		if (sf::Event::Closed == event.type)
		{
			m_window.close();
		}

		if (sf::Event::KeyPressed == event.type)
		{
			if (sf::Keyboard::Escape == event.key.code)
			{
				m_exitGame = true;
			}
		}
	}
}

/// <summary>
/// Update the game world
/// </summary>
/// <param name="t_deltaTime">Time interval per frame</param>
void Game::update(sf::Time t_deltaTime)
{
	gameStateManager(m_gameState);
}

/// <summary>
/// Draw the frame and then switch buffers
/// </summary>
void Game::render()
{
	m_window.clear(sf::Color::Black);
	drawScreen(m_gameState);
	m_window.display();
}

/// <summary>
/// Load the font and setup the text message for displaying on-screen
/// </summary>
void Game::setupFontAndText()
{
	m_normalFont.loadFromFile("ASSETS\\FONTS\\OratorStd.otf");

	m_displayText.setFont(m_normalFont);
	m_displayText.setString("");
	m_displayText.setCharacterSize(22);
	m_displayText.setFillColor(sf::Color::White);

	m_yellowOrbsReqText.setFont(m_normalFont);
	m_yellowOrbsReqText.setString(std::to_string(m_yellowOrbsReq));
	m_yellowOrbsReqText.setCharacterSize(22);
	m_yellowOrbsReqText.setFillColor(sf::Color::White);

	m_purpleOrbsReqText.setFont(m_normalFont);
	m_purpleOrbsReqText.setString(std::to_string(m_purpleOrbsReq));
	m_purpleOrbsReqText.setCharacterSize(22);
	m_purpleOrbsReqText.setFillColor(sf::Color::White);

	m_greenOrbsReqText.setFont(m_normalFont);
	m_greenOrbsReqText.setString(std::to_string(m_greenOrbsReq));
	m_greenOrbsReqText.setCharacterSize(22);
	m_greenOrbsReqText.setFillColor(sf::Color::White);

	m_yellowOrbsReqText.setPosition(424, 425); // Yellow orbs required on map screen
	m_purpleOrbsReqText.setPosition(476, 425); // Purple orbs required on map screen
	m_greenOrbsReqText.setPosition(528, 425); // Green orbs required on map screen

	m_livesUIText.setFont(m_normalFont);
	m_livesUIText.setPosition(40, 550);
	m_livesUIText.setCharacterSize(15);
	m_livesUIText.setFillColor(sf::Color::Black);

	m_yelOrbsUIText.setFont(m_normalFont);
	m_yelOrbsUIText.setPosition(601, 550);
	m_yelOrbsUIText.setCharacterSize(15);
	m_yelOrbsUIText.setFillColor(sf::Color::Black);

	m_redOrbsUIText.setFont(m_normalFont);
	m_redOrbsUIText.setPosition(637, 550);
	m_redOrbsUIText.setCharacterSize(15);
	m_redOrbsUIText.setFillColor(sf::Color::Black);

	m_purpOrbsUIText.setFont(m_normalFont);
	m_purpOrbsUIText.setPosition(673, 550);
	m_purpOrbsUIText.setCharacterSize(15);
	m_purpOrbsUIText.setFillColor(sf::Color::Black);

	m_greenOrbsUIText.setFont(m_normalFont);
	m_greenOrbsUIText.setPosition(709, 550);
	m_greenOrbsUIText.setCharacterSize(15);
	m_greenOrbsUIText.setFillColor(sf::Color::Black);

	m_scrapOrbsUIText.setFont(m_normalFont);
	m_scrapOrbsUIText.setPosition(745, 550);
	m_scrapOrbsUIText.setCharacterSize(15);
	m_scrapOrbsUIText.setFillColor(sf::Color::Black);

	m_upgradeCostText.setFont(m_normalFont);
	m_upgradeCostText.setCharacterSize(22);
	m_upgradeCostText.setFillColor(sf::Color::White);

	// Used on market screen
	m_yellowOrbsText.setFont(m_normalFont);
	m_yellowOrbsText.setPosition(393, 381);
	m_yellowOrbsText.setCharacterSize(14);
	m_yellowOrbsText.setFillColor(sf::Color::White);

	m_redOrbsText.setFont(m_normalFont);
	m_redOrbsText.setPosition(433, 381);
	m_redOrbsText.setCharacterSize(14);
	m_redOrbsText.setFillColor(sf::Color::White);

	m_purpleOrbsText.setFont(m_normalFont);
	m_purpleOrbsText.setPosition(473, 381);
	m_purpleOrbsText.setCharacterSize(14);
	m_purpleOrbsText.setFillColor(sf::Color::White);

	m_greenOrbsText.setFont(m_normalFont);
	m_greenOrbsText.setPosition(513, 381);
	m_greenOrbsText.setCharacterSize(14);
	m_greenOrbsText.setFillColor(sf::Color::White);

	m_scrapOrbsText.setFont(m_normalFont);
	m_scrapOrbsText.setPosition(553, 381);
	m_scrapOrbsText.setCharacterSize(14);
	m_scrapOrbsText.setFillColor(sf::Color::White);
}

/// <summary>
/// Load textures
/// </summary>
void Game::loadTexturesAndSound()
{
	// Textures
	m_splashScreenTexture.loadFromFile("ASSETS\\IMAGES\\splash_screen.png");
	m_splashScreenSprite.setTexture(m_splashScreenTexture);
	m_splashScreenSprite.setPosition(0, 0);

	m_mainMenuTexture.loadFromFile("ASSETS\\IMAGES\\main_menu.png");
	m_mainMenuSprite.setTexture(m_mainMenuTexture);
	m_mainMenuSprite.setPosition(0, 0);

	m_mapScreenTexture.loadFromFile("ASSETS\\IMAGES\\map_screen.png");
	m_mapScreenSprite.setTexture(m_mapScreenTexture);
	m_mapScreenSprite.setPosition(0, 0);

	m_hangarScreenTexture.loadFromFile("ASSETS\\IMAGES\\hangar_screen.png");
	m_hangarScreenSprite.setTexture(m_hangarScreenTexture);
	m_hangarScreenSprite.setPosition(0, 0);

	m_marketScreenTexture.loadFromFile("ASSETS\\IMAGES\\market_screen.png");
	m_marketScreenSprite.setTexture(m_marketScreenTexture);
	m_marketScreenSprite.setPosition(0, 0);

	m_helpScreenTexture.loadFromFile("ASSETS\\IMAGES\\help_screen.png");
	m_helpScreenSprite.setTexture(m_helpScreenTexture);
	m_helpScreenSprite.setPosition(0, 0);

	m_livesUITexture.loadFromFile("ASSETS\\SPRITES\\lives_ui.png");
	m_livesUISprite.setTexture(m_livesUITexture);
	m_livesUISprite.setPosition(20, 510);

	m_orbsUITexture.loadFromFile("ASSETS\\SPRITES\\orbs_ui.png");
	m_orbsUISprite.setTexture(m_orbsUITexture);
	m_orbsUISprite.setPosition(580, 510);

	m_venusLockedTex.loadFromFile("ASSETS\\IMAGES\\venus_locked.png");
	m_venusLockedSprite.setTexture(m_venusLockedTex);

	m_earthLockedTex.loadFromFile("ASSETS\\IMAGES\\earth_locked.png");
	m_earthLockedSprite.setTexture(m_earthLockedTex);

	m_moonLockedTex.loadFromFile("ASSETS\\IMAGES\\moon_locked.png");
	m_moonLockedSprite.setTexture(m_moonLockedTex);

	m_marsLockedTex.loadFromFile("ASSETS\\IMAGES\\mars_locked.png");
	m_marsLockedSprite.setTexture(m_marsLockedTex);

	m_jupiterLockedTex.loadFromFile("ASSETS\\IMAGES\\jupiter_locked.png");
	m_jupiterLockedSprite.setTexture(m_jupiterLockedTex);

	m_saturnLockedTex.loadFromFile("ASSETS\\IMAGES\\saturn_locked.png");
	m_saturnLockedSprite.setTexture(m_saturnLockedTex);

	m_uranusLockedTex.loadFromFile("ASSETS\\IMAGES\\uranus_locked.png");
	m_uranusLockedSprite.setTexture(m_uranusLockedTex);

	m_neptuneLockedTex.loadFromFile("ASSETS\\IMAGES\\neptune_locked.png");
	m_neptuneLockedSprite.setTexture(m_neptuneLockedTex);

	m_pauseScreenTex.loadFromFile("ASSETS\\IMAGES\\pause_screen.png");
	m_pauseScreenSprite.setTexture(m_pauseScreenTex);
	m_pauseScreenSprite.setPosition(300, 250);

	m_gameOverScreenTex.loadFromFile("ASSETS\\IMAGES\\game_over_screen.png");
	m_gameOverScreenSprite.setTexture(m_gameOverScreenTex);
	m_gameOverScreenSprite.setPosition(300, 250);

	// Sound
	m_asteroidBreakBuf.loadFromFile("ASSETS\\AUDIO\\asteroid_breaking.wav");
	m_asteroidBreakSound.setBuffer(m_asteroidBreakBuf);

	m_explosionBuf.loadFromFile("ASSETS\\AUDIO\\explosion.wav");
	m_explosionSound.setBuffer(m_explosionBuf);

	m_uiBackBuf.loadFromFile("ASSETS\\AUDIO\\ui_back.wav");
	m_uiBackSound.setBuffer(m_uiBackBuf);

	m_uiSelectBuf.loadFromFile("ASSETS\\AUDIO\\ui_select.wav");
	m_uiSelectSound.setBuffer(m_uiSelectBuf);

	m_uiErrorBuf.loadFromFile("ASSETS\\AUDIO\\error_sound.wav");
	m_uiErrorSound.setBuffer(m_uiErrorBuf);
	m_uiErrorSound.setVolume(45.0f);

	m_uiSuccessBuf.loadFromFile("ASSETS\\AUDIO\\success_sound.wav");
	m_uiSuccessSound.setBuffer(m_uiSuccessBuf);
	m_uiSuccessSound.setVolume(45.0f);
}

// Game play input
void Game::gameControls()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		m_playerShip.rotateLeft();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		m_playerShip.rotateRight();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		m_playerShip.increaseVelocity();
		m_playerShip.animate();
	}
	else
	{
		m_playerShip.idle();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && !m_bulletFired)
	{
		m_playerShip.fire();
		m_bulletFired = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
	{
		m_gamePaused = true;
	}
}

// Looks after game logic
void Game::gameStateManager(GameState t_gameState)
{
	switch (t_gameState)
	{
	case GameState::SPLASH_SCREEN: // Show splash screen until SPACE is pressed

		while (m_keyNotPressed)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				m_keyNotPressed = false;
			}
		}
		m_gameState = GameState::MAIN_MENU;
		break;

	case GameState::MAIN_MENU: // Show main menu and wait for mouse click

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window

			// MAP Button
			if ((m_mousePosition.x > 192 && m_mousePosition.x < 371)
				&& (m_mousePosition.y > 224 && m_mousePosition.y < 277))
			{
				m_uiSelectSound.play();
				m_gameState = GameState::MAP_SCREEN;
			}

			// HANGAR Button
			if (m_mousePosition.x > 434 && m_mousePosition.x < 612
				&& m_mousePosition.y > 224 && m_mousePosition.y < 277)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::HANGAR_SCREEN;
			}
			
			// MARKET Button
			if (m_mousePosition.x > 192 && m_mousePosition.x < 371
				&& m_mousePosition.y > 316 && m_mousePosition.y < 369)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::MARKET_SCREEN;
			}

			// HELP Button
			if (m_mousePosition.x > 434 && m_mousePosition.x < 612
				&& m_mousePosition.y > 316 && m_mousePosition.y < 369)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::HELP_SCREEN;
			}
		}
		break;

	case MAP_SCREEN:

		m_displayText.setPosition(152, 335); // Set position of text for displaying selected planet
		m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window
		setupAsteroids(); // Reset for next game
		m_playerShip.reset(); // Reset for next game
		resetEnemiesAndExplosions();
		checkPlanetUnlocks(); // Check total game lifetime orbs collected and unlock planets accordingly

		for (int i = 0; i < MAX_BULLETS; i++)
		{
			m_playerShip.m_bullets[i].kill();
		}
																
		// MERCURY
		if ((m_mousePosition.x > 165 && m_mousePosition.x < 186)
			&& (m_mousePosition.y > 286 && m_mousePosition.y < 307))
		{
			m_displayText.setString("MERCURY");
			m_yellowOrbsReqText.setString("00");
			m_purpleOrbsReqText.setString("00");
			m_greenOrbsReqText.setString("00");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}
		}

		// VENUS
		if ((m_mousePosition.x > 202 && m_mousePosition.x < 232)
			&& (m_mousePosition.y > 277 && m_mousePosition.y < 307))
		{
			m_displayText.setString("VENUS");
			m_yellowOrbsReqText.setString("03");
			m_purpleOrbsReqText.setString("02");
			m_greenOrbsReqText.setString("03");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) & m_isVenusUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isVenusUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// EARTH
		if ((m_mousePosition.x > 248 && m_mousePosition.x < 290)
			&& (m_mousePosition.y > 264 && m_mousePosition.y < 307))
		{
			m_displayText.setString("EARTH");
			m_yellowOrbsReqText.setString("05");
			m_purpleOrbsReqText.setString("03");
			m_greenOrbsReqText.setString("09");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isEarthUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isEarthUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// MOON
		if ((m_mousePosition.x > 285 && m_mousePosition.x < 294)
			&& (m_mousePosition.y > 255 && m_mousePosition.y < 263))
		{
			m_displayText.setString("THE MOON");
			m_yellowOrbsReqText.setString("08");
			m_purpleOrbsReqText.setString("05");
			m_greenOrbsReqText.setString("10");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isMoonUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isMoonUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// MARS
		if ((m_mousePosition.x > 308 && m_mousePosition.x < 348)
			&& (m_mousePosition.y > 267 && m_mousePosition.y < 307))
		{
			m_displayText.setString("MARS");
			m_yellowOrbsReqText.setString("13");
			m_purpleOrbsReqText.setString("12");
			m_greenOrbsReqText.setString("15");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isMarsUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isMarsUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// JUPITER
		if ((m_mousePosition.x > 363 && m_mousePosition.x < 425)
			&& (m_mousePosition.y > 245 && m_mousePosition.y < 307))
		{
			m_displayText.setString("JUPITER");
			m_yellowOrbsReqText.setString("21");
			m_purpleOrbsReqText.setString("23");
			m_greenOrbsReqText.setString("25");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isJupiterUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isJupiterUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// SATURN
		if ((m_mousePosition.x > 464 && m_mousePosition.x < 509)
			&& (m_mousePosition.y > 263 && m_mousePosition.y < 307))
		{
			m_displayText.setString("SATURN");
			m_yellowOrbsReqText.setString("33");
			m_purpleOrbsReqText.setString("30");
			m_greenOrbsReqText.setString("27");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isSaturnUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isSaturnUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// URANUS
		if ((m_mousePosition.x > 536 && m_mousePosition.x < 568)
			&& (m_mousePosition.y > 277 && m_mousePosition.y < 307))
		{
			m_displayText.setString("URANUS");
			m_yellowOrbsReqText.setString("46");
			m_purpleOrbsReqText.setString("48");
			m_greenOrbsReqText.setString("43");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isUranusUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isUranusUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
		}

		// NEPTUNE
		if ((m_mousePosition.x > 591 && m_mousePosition.x < 635)
			&& (m_mousePosition.y > 265 && m_mousePosition.y < 307))
		{
			m_displayText.setString("NEPTUNE");
			m_yellowOrbsReqText.setString("81");
			m_purpleOrbsReqText.setString("83");
			m_greenOrbsReqText.setString("82");

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_isNeptuneUnlocked)
			{
				m_uiSelectSound.play();
				m_gameState = GameState::PLAY_GAME;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_isNeptuneUnlocked)
			{
				m_uiErrorSound.play();
				sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
			}
			
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			// BACK Button
			if ((m_mousePosition.x > 136 && m_mousePosition.x < 239)
				&& (m_mousePosition.y > 392 && m_mousePosition.y < 445))
			{
				m_uiBackSound.play();
				m_gameState = GameState::MAIN_MENU;
			}
		}
		break;

	case HANGAR_SCREEN:

		m_upgradeCostText.setPosition(380.0f, 368.0f); // Text for upgrade cost
		m_upgradeCostText.setString(std::to_string(m_upgradeCost) + " sc");
		m_displayText.setPosition(380.0f, 413.0f); // Text for current cash
		m_displayText.setString(std::to_string(m_currentCash) + " sc");
		m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window
		
		// LASER buy button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 232 && m_mousePosition.y < 253))
		{
			switch (m_laserUpgradeLevel)
			{
			case 1: // Level 1
				m_upgradeCost = 1000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1000)
					{
						m_currentCash -= 1000;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 2
						delayTime = LASER_LVL_2; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 2: // Level 2
				m_upgradeCost = 1500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1500)
					{
						m_currentCash -= 1500;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 3
						delayTime = LASER_LVL_3; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 3: // Level 3
				m_upgradeCost = 2000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 2000)
					{
						m_currentCash -= 2000;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 4
						delayTime = LASER_LVL_4; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 4: // Level 4
				m_upgradeCost = 3000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 3000)
					{
						m_currentCash -= 3000;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 5
						delayTime = LASER_LVL_5; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 5: // Level 5
				m_upgradeCost = 4500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 4500)
					{
						m_currentCash -= 4500;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 6
						delayTime = LASER_LVL_6; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 6: // Level 6
				m_upgradeCost = 7000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 7000)
					{
						m_currentCash -= 7000;
						m_laserUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_laserUpgradeBar.setSize(sf::Vector2f(m_laserUpgradeBarLength, 25));
						m_laserUpgradeLevel++; // Upgrade to level 7
						delayTime = LASER_LVL_7; // Upgrade shooting speed
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 7: // Level 7
				m_upgradeCost = 0;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
				break;
			}
		}

		// HOLD buy button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 260 && m_mousePosition.y < 280))
		{
			switch (m_holdUpgradeLevel)
			{
			case 1: // Level 1
				m_upgradeCost = 1000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1000)
					{
						m_currentCash -= 1000;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 2
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 2: // Level 2
				m_upgradeCost = 1500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1500)
					{
						m_currentCash -= 1500;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 3
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 3: // Level 3
				m_upgradeCost = 2000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 2000)
					{
						m_currentCash -= 2000;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 4
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 4: // Level 4
				m_upgradeCost = 3000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 3000)
					{
						m_currentCash -= 3000;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 5
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 5: // Level 5
				m_upgradeCost = 4500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 4500)
					{
						m_currentCash -= 4500;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 6
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 6: // Level 6
				m_upgradeCost = 7000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 7000)
					{
						m_currentCash -= 7000;
						m_holdUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_holdUpgradeBar.setSize(sf::Vector2f(m_holdUpgradeBarLength, 25));
						m_holdUpgradeLevel++; // Upgrade to level 7
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 7: // Level 7
				m_upgradeCost = 0;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
				break;
			}
		}

		// ENGINE buy button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 290 && m_mousePosition.y < 310))
		{
			switch (m_engineUpgradeLevel)
			{
			case 1: // Level 1
				m_upgradeCost = 1000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1000)
					{
						m_currentCash -= 1000;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 2
						m_playerShip.upgradeThrust(THRUST_LVL_2); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 2: // Level 2
				m_upgradeCost = 1500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1500)
					{
						m_currentCash -= 1500;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 3
						m_playerShip.upgradeThrust(THRUST_LVL_3); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 3: // Level 3
				m_upgradeCost = 2000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 2000)
					{
						m_currentCash -= 2000;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 4
						m_playerShip.upgradeThrust(THRUST_LVL_4); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 4: // Level 4
				m_upgradeCost = 3000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 3000)
					{
						m_currentCash -= 3000;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 5
						m_playerShip.upgradeThrust(THRUST_LVL_5); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 5: // Level 5
				m_upgradeCost = 4500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 4500)
					{
						m_currentCash -= 4500;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 6
						m_playerShip.upgradeThrust(THRUST_LVL_6); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 6: // Level 6
				m_upgradeCost = 7000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 7000)
					{
						m_currentCash -= 7000;
						m_engineUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_engineUpgradeBar.setSize(sf::Vector2f(m_engineUpgradeBarLength, 25));
						m_engineUpgradeLevel++; // Upgrade to level 7
						m_playerShip.upgradeThrust(THRUST_LVL_7); // Upgrade ship engine
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 7: // Level 7
				m_upgradeCost = 0;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
				break;
			}
		}
			
		// REACTOR buy button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 320 && m_mousePosition.y < 340))
		{
			switch (m_reactorUpgradeLevel)
			{
			case 1: // Level 1
				m_upgradeCost = 1000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1000)
					{
						m_currentCash -= 1000;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 2
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
			break;

			case 2: // Level 2
				m_upgradeCost = 1500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 1500)
					{
						m_currentCash -= 1500;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 3
						m_playerShip.upgradeLives(); // Permanently increase lives by 1
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 3: // Level 3
				m_upgradeCost = 2000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 2000)
					{
						m_currentCash -= 2000;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 4
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 4: // Level 4
				m_upgradeCost = 3000;
				
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 3000)
					{
						m_currentCash -= 3000;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 5
						m_playerShip.upgradeLives(); // Permanently increase lives by 1
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 5: // Level 5
				m_upgradeCost = 4500;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 4500)
					{
						m_currentCash -= 4500;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 6
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 6: // Level 6
				m_upgradeCost = 7000;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (m_currentCash >= 7000)
					{
						m_currentCash -= 7000;
						m_reactorUpgradeBarLength += UPGRADE_BAR_INCREMENT;
						m_reactorUpgradeBar.setSize(sf::Vector2f(m_reactorUpgradeBarLength, 25));
						m_reactorUpgradeLevel++; // Upgrade to level 7
						m_playerShip.upgradeLives(); // Permanently increase lives by 1
						sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
						m_uiSuccessSound.play();
					}
					else
					{
						m_uiErrorSound.play();
						sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
					}
				}
				break;

			case 7: // Level 7
				m_upgradeCost = 0;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
				break;
			}
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			// BACK Button
			if ((m_mousePosition.x > 136 && m_mousePosition.x < 239)
				&& (m_mousePosition.y > 392 && m_mousePosition.y < 445))
			{
				m_uiBackSound.play();
				m_gameState = GameState::MAIN_MENU;
			}
		}
		break;

	case MARKET_SCREEN:
		m_yellowOrbsText.setString(std::to_string(m_inventory.getYellowOrbs()));
		m_redOrbsText.setString(std::to_string(m_inventory.getRedOrbs()));
		m_purpleOrbsText.setString(std::to_string(m_inventory.getPurpleOrbs()));
		m_greenOrbsText.setString(std::to_string(m_inventory.getGreenOrbs()));
		m_scrapOrbsText.setString(std::to_string(m_inventory.getScrapOrbs()));
		m_displayText.setPosition(380.0f, 413.0f); // Set position of text for displaying upgrade cost
		m_displayText.setString(std::to_string(m_currentCash) + " sc");
		m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window

		// COLOUR CODE: Yellow (0) - Red (1) - Purple (2) - Green (3) - Scrap (4)

		// Buy 500 sc button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 232 && m_mousePosition.y < 253))
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (m_inventory.getYellowOrbs() >= 1 && m_inventory.getRedOrbs() >= 2)
				{
					m_inventory.decreaseOrbs(0, 1);
					m_inventory.decreaseOrbs(1, 2);
					m_currentCash += 500;
					sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
					m_uiSuccessSound.play();
				}
				else
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
			}
		}

		// Buy 1000 sc button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 260 && m_mousePosition.y < 280))
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (m_inventory.getYellowOrbs() >= 1 && m_inventory.getRedOrbs() >= 3
					&& m_inventory.getGreenOrbs() >= 2)
				{
					m_inventory.decreaseOrbs(0, 1);
					m_inventory.decreaseOrbs(1, 3);
					m_inventory.decreaseOrbs(3, 2);
					m_currentCash += 1000;
					sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
					m_uiSuccessSound.play();
				}
				else
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
			}
		}

		// Buy 2000 sc button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 290 && m_mousePosition.y < 310))
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (m_inventory.getRedOrbs() >= 2 && m_inventory.getPurpleOrbs() >= 2
					&& m_inventory.getGreenOrbs() >= 2)
				{
					m_inventory.decreaseOrbs(1, 2);
					m_inventory.decreaseOrbs(2, 2);
					m_inventory.decreaseOrbs(3, 2);
					m_currentCash += 2000;
					sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
					m_uiSuccessSound.play();
				}
				else
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
			}
		}

		// Buy 2500 sc button
		if ((m_mousePosition.x > 582 && m_mousePosition.x < 632)
			&& (m_mousePosition.y > 320 && m_mousePosition.y < 340))
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (m_inventory.getYellowOrbs() >= 1 && m_inventory.getRedOrbs() >= 3
					&& m_inventory.getGreenOrbs() >= 2)
				{
					m_inventory.decreaseOrbs(0, 1);
					m_inventory.decreaseOrbs(1, 3);
					m_inventory.decreaseOrbs(3, 2);
					m_currentCash += 2500;
					sf::sleep(sf::seconds(1.0f)); // Prevent multiple mouse clicks
					m_uiSuccessSound.play();
				}
				else
				{
					m_uiErrorSound.play();
					sf::sleep(sf::seconds(0.5f)); // Prevent multiple mouse clicks
				}
			}
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			// BACK Button
			if ((m_mousePosition.x > 136 && m_mousePosition.x < 239)
				&& (m_mousePosition.y > 392 && m_mousePosition.y < 445))
			{
				m_uiBackSound.play();
				m_gameState = GameState::MAIN_MENU;
			}
		}

		break;

	case HELP_SCREEN:

		m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			// BACK Button
			if ((m_mousePosition.x > 136 && m_mousePosition.x < 239)
				&& (m_mousePosition.y > 392 && m_mousePosition.y < 445))
			{
				m_uiBackSound.play();
				m_gameState = GameState::MAIN_MENU;
			}
		}
		break;

	case PLAY_GAME:
		gamePlayMode();
	}
}

// Looks after drawing everything
void Game::drawScreen(GameState t_gameState)
{
	switch (t_gameState)
	{
	case GameState::SPLASH_SCREEN:
		m_window.draw(m_splashScreenSprite);
		break;

	case GameState::MAIN_MENU:
		m_window.draw(m_mainMenuSprite);
		break;

	case GameState::MAP_SCREEN:
		m_window.draw(m_mapScreenSprite);
		m_window.draw(m_displayText);
		m_window.draw(m_yellowOrbsReqText);
		m_window.draw(m_purpleOrbsReqText);
		m_window.draw(m_greenOrbsReqText);
		
		if (!m_isVenusUnlocked)
		{
			m_window.draw(m_venusLockedSprite);
		}

		if (!m_isEarthUnlocked)
		{
			m_window.draw(m_earthLockedSprite);
		}

		if (!m_isMoonUnlocked)
		{
			m_window.draw(m_moonLockedSprite);
		}

		if (!m_isMarsUnlocked)
		{
			m_window.draw(m_marsLockedSprite);
		}

		if (!m_isJupiterUnlocked)
		{
			m_window.draw(m_jupiterLockedSprite);
		}

		if (!m_isSaturnUnlocked)
		{
			m_window.draw(m_saturnLockedSprite);
		}

		if (!m_isUranusUnlocked)
		{
			m_window.draw(m_uranusLockedSprite);
		}

		if (!m_isNeptuneUnlocked)
		{
			m_window.draw(m_neptuneLockedSprite);
		}

		break;

	case GameState::HANGAR_SCREEN:
		m_window.draw(m_laserUpgradeBar);
		m_window.draw(m_holdUpgradeBar);
		m_window.draw(m_engineUpgradeBar);
		m_window.draw(m_reactorUpgradeBar);
		m_window.draw(m_hangarScreenSprite);
		m_window.draw(m_upgradeCostText);
		m_window.draw(m_displayText);
		break;

	case GameState::MARKET_SCREEN:
		m_window.draw(m_marketScreenSprite);
		m_window.draw(m_displayText);
		m_window.draw(m_yellowOrbsText);
		m_window.draw(m_redOrbsText);
		m_window.draw(m_purpleOrbsText);
		m_window.draw(m_greenOrbsText);
		m_window.draw(m_scrapOrbsText);
		break;

	case GameState::HELP_SCREEN:
		m_window.draw(m_helpScreenSprite);
		break;

	case GameState::PLAY_GAME:
		// Check if an orb is active and draw
		for (int i = 0; i < MAX_ORBS; i++)
		{
			if (m_orbs[i].isAlive())
			{
				m_window.draw(m_orbs[i].getSprite());
			}
		}

		m_window.draw(m_playerShip.getSprite());
		
		if (m_enemyShip_1.isAlive()) // Enemy 1
		{
			m_window.draw(m_enemyShip_1.getSprite());
		}

		if (m_enemyShip_2.isAlive()) // Enemy 2
		{
			m_window.draw(m_enemyShip_2.getSprite());
		}

		// Check if player / enemy bullets are active and draw
		for (int i = 0; i < MAX_BULLETS; i++)
		{
			if (m_playerShip.m_bullets[i].isAlive())
			{
				m_window.draw(m_playerShip.m_bullets[i].getSprite());
			}

			if (m_enemyShip_1.m_bullets[i].isAlive())
			{
				m_window.draw(m_enemyShip_1.m_bullets[i].getSprite());
			}

			if (m_enemyShip_2.m_bullets[i].isAlive())
			{
				m_window.draw(m_enemyShip_2.m_bullets[i].getSprite());
			}
		}

		// Check if asteroids are active and draw
		for (int i = 0; i < MAX_ASTEROIDS; i++)
		{
			if (m_asteroids[i].isAlive())
			{
				m_window.draw(m_asteroids[i].getSprite());
			}
		}

		// Check if medium asteroids are active and draw
		for (int i = 0; i < MAX_MEDIUM_ASTEROIDS; i++)
		{
			if (m_asteroidsMedium[i].isAlive())
			{
				m_window.draw(m_asteroidsMedium[i].getSprite());
			}
		}

		// Check if small asteroids are active and draw
		for (int i = 0; i < MAX_SMALL_ASTEROIDS; i++)
		{
			if (m_asteroidsSmall[i].isAlive())
			{
				m_window.draw(m_asteroidsSmall[i].getSprite());
			}
		}

		// Check if an explosion is active and draw
		for (int i = 0; i < MAX_EXPLOSIONS; i++)
		{
			if (m_explosions[i].isAlive())
			{
				m_window.draw(m_explosions[i].getSprite());
			}
		}

		// Draw ui over everything else
		showUI();

		// Draw pause screen over everything
		if (m_gamePaused)
		{
			m_window.draw(m_pauseScreenSprite);
		}

		// Draw game over screen if death has occurred
		if (m_gameOver)
		{
			m_window.draw(m_gameOverScreenSprite);
		}
		break;
	}
}

// This is the actual game
void Game::gamePlayMode()
{
	if (!m_gamePaused && !m_gameOver)
	{
		checkIfPlayerIsAlive();
		gameControls();
		m_playerShip.move(); // Ship movement
		m_enemyShip_1.move(m_playerShip.getPosition());
		m_enemyShip_2.move(m_playerShip.getPosition());
		collisionDetection();

		// Asteroids movement
		for (int i = 0; i < MAX_ASTEROIDS; i++)
		{
			m_asteroids[i].update(m_playerShip.getPosition());
		}

		// Medium asteroids movement
		for (int i = 0; i < MAX_MEDIUM_ASTEROIDS; i++)
		{
			m_asteroidsMedium[i].update(m_playerShip.getPosition());
		}

		// Small asteroids movement
		for (int i = 0; i < MAX_SMALL_ASTEROIDS; i++)
		{
			m_asteroidsSmall[i].update(m_playerShip.getPosition());
		}

		// If a bullet is alive then move it
		for (int i = 0; i < MAX_BULLETS; i++)
		{
			m_playerShip.m_bullets[i].update();
			m_enemyShip_1.m_bullets[i].update();
			m_enemyShip_2.m_bullets[i].update();
		}

		// If an explosion is alive then animate it
		for (int i = 0; i < MAX_EXPLOSIONS; i++)
		{
			m_explosions[i].update();
		}

		// Check for orb collection
		for (int i = 0; i < MAX_ORBS; i++)
		{
			if (m_orbs[i].isAlive())
			{
				m_orbs[i].collectOrb(m_playerShip.getPosition(), m_inventory, m_holdUpgradeLevel);
			}
		}
	}
	else
	{
		if (m_gamePaused)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				m_gamePaused = false;
			}

			m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				// QUIT Button
				if ((m_mousePosition.x > 324 && m_mousePosition.x < 474)
					&& (m_mousePosition.y > 258 && m_mousePosition.y < 311))
				{
					m_uiBackSound.play();
					m_gamePaused = false;
					m_gameState = GameState::MAIN_MENU;
				}
			}
		}

		if (m_gameOver)
		{
			m_mousePosition = sf::Mouse::getPosition(m_window); // Get mouse position relative to game window

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				// QUIT Button
				if ((m_mousePosition.x > 324 && m_mousePosition.x < 474)
					&& (m_mousePosition.y > 258 && m_mousePosition.y < 311))
				{
					m_uiBackSound.play();
					m_gameOver = false;
					m_gameState = GameState::MAIN_MENU;
				}
			}
		}
	}
}

// Detect collisions between player and asteroids
// I really should tidy up this code but it works so I'll leave it for now (and forever)
void Game::collisionDetection()
{
	for (int i = 0; i < MAX_ASTEROIDS; i++) // Large asteroids and player collision
	{
		if (m_asteroids[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_asteroids[i].getPosition(), m_playerShip.getPosition());

			if (m_distance < m_asteroids[i].getRadius() + 32)
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_asteroids[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_asteroids[i].getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_playerShip.loseLife();
			}
		}
	}

	for (int i = 0; i < MAX_MEDIUM_ASTEROIDS; i++) // Medium asteroids and player collision
	{
		if (m_asteroidsMedium[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_asteroidsMedium[i].getPosition(), m_playerShip.getPosition());

			if (m_distance < m_asteroidsMedium[i].getRadius() + 32)
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_asteroidsMedium[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_asteroidsMedium[i].getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_playerShip.loseLife();
			}
		}
	}

	for (int i = 0; i < MAX_SMALL_ASTEROIDS; i++) // Small asteroids and player collision
	{
		if (m_asteroidsSmall[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_asteroidsSmall[i].getPosition(), m_playerShip.getPosition());

			if (m_distance < m_asteroidsSmall[i].getRadius() + 32)
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_asteroidsSmall[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_asteroidsSmall[i].getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_playerShip.loseLife();
			}
		}
	}

	// Check for collision between asteroids and bullets
	for (int i = 0; i < MAX_ASTEROIDS; i++)
	{
		if (m_asteroids[i].isAlive()) // Large asteroids and bullets collision
		{
			for (int j = 0; j < MAX_BULLETS; j++)
			{
				if (m_playerShip.m_bullets[j].isAlive())
				{
					m_distance = getDistanceBetweenTwoPoints(m_asteroids[i].getPosition(), m_playerShip.m_bullets[j].getPosition());

					if (m_distance < m_asteroids[i].getRadius() + BULLET_RADIUS)
					{
						m_asteroids[i].kill();
						m_playerShip.m_bullets[j].kill();
						m_explosions[m_explosionArrayPos].setExplosion(m_asteroids[i].getPosition());
						m_explosionSound.play();
						m_explosionArrayPos++;

						if (m_explosionArrayPos > MAX_EXPLOSIONS)
						{
							m_explosionArrayPos = 0;
						}

						if (m_asteroidsMediumArrayPos > MAX_MEDIUM_ASTEROIDS)
						{
							m_asteroidsMediumArrayPos = 0;
						}

						m_asteroidsMedium[m_asteroidsMediumArrayPos].setAsteroid(m_asteroids[i].getPosition(), m_asteroids[i].getDirection());
						m_asteroidsMedium[m_asteroidsMediumArrayPos].activate();
						m_asteroidsMediumArrayPos++;

						if (m_asteroidsMediumArrayPos > MAX_MEDIUM_ASTEROIDS)
						{
							m_asteroidsMediumArrayPos = 0;
						}

						m_asteroidsMedium[m_asteroidsMediumArrayPos].setAsteroid(m_asteroids[i].getPosition(), m_asteroids[i].getOppositeDirection());
						m_asteroidsMedium[m_asteroidsMediumArrayPos].activate();
						m_asteroidsMediumArrayPos++;

						if (m_asteroidsMediumArrayPos > MAX_MEDIUM_ASTEROIDS)
						{
							m_asteroidsMediumArrayPos = 0;
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < MAX_MEDIUM_ASTEROIDS; i++) // Medium asteroids and bullets collision
	{
		if (m_asteroidsMedium[i].isAlive())
		{
			for (int j = 0; j < MAX_BULLETS; j++)
			{
				if (m_playerShip.m_bullets[j].isAlive())
				{
					m_distance = getDistanceBetweenTwoPoints(m_asteroidsMedium[i].getPosition(), m_playerShip.m_bullets[j].getPosition());

					if (m_distance < m_asteroidsMedium[i].getRadius() + BULLET_RADIUS)
					{
						m_asteroidsMedium[i].kill();
						m_playerShip.m_bullets[j].kill();
						m_explosions[m_explosionArrayPos].setExplosion(m_asteroidsMedium[i].getPosition());
						m_explosionSound.play();
						m_explosionArrayPos++;

						if (m_explosionArrayPos > MAX_EXPLOSIONS)
						{
							m_explosionArrayPos = 0;
						}

						if (m_asteroidsSmallArrayPos > MAX_SMALL_ASTEROIDS)
						{
							m_asteroidsSmallArrayPos = 0;
						}

						m_asteroidsSmall[m_asteroidsSmallArrayPos].setAsteroid(m_asteroidsMedium[i].getPosition(), m_asteroids[i].getDirection());
						m_asteroidsSmall[m_asteroidsSmallArrayPos].activate();
						m_asteroidsSmallArrayPos++;

						if (m_asteroidsSmallArrayPos > MAX_SMALL_ASTEROIDS)
						{
							m_asteroidsSmallArrayPos = 0;
						}

						m_asteroidsSmall[m_asteroidsSmallArrayPos].setAsteroid(m_asteroidsMedium[i].getPosition(), m_asteroids[i].getOppositeDirection());
						m_asteroidsSmall[m_asteroidsSmallArrayPos].activate();
						m_asteroidsSmallArrayPos++;

						if (m_asteroidsSmallArrayPos > MAX_SMALL_ASTEROIDS)
						{
							m_asteroidsSmallArrayPos = 0;
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < MAX_SMALL_ASTEROIDS; i++) // Small asteroids and bullets collision
	{
		if (m_asteroidsSmall[i].isAlive())
		{
			for (int j = 0; j < MAX_BULLETS; j++)
			{
				if (m_playerShip.m_bullets[j].isAlive())
				{
					m_distance = getDistanceBetweenTwoPoints(m_asteroidsSmall[i].getPosition(), m_playerShip.m_bullets[j].getPosition());

					if (m_distance < m_asteroidsSmall[i].getRadius() + BULLET_RADIUS)
					{
						m_asteroidsSmall[i].kill();
						m_playerShip.m_bullets[j].kill();
						m_explosions[m_explosionArrayPos].setExplosion(m_asteroidsSmall[i].getPosition());
						m_explosionSound.play();
						m_explosionArrayPos++;

						if (m_explosionArrayPos > MAX_EXPLOSIONS)
						{
							m_explosionArrayPos = 0;
						}
						
						generateOrb(m_asteroidsSmall[i].getPosition());
					}
				}
			}
		}
	}

	for (int i = 0; i < MAX_BULLETS; i++) // Enemy bullets and player collision, player bullets and enemy collision
	{
		if (m_enemyShip_1.m_bullets[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_enemyShip_1.m_bullets[i].getPosition(), m_playerShip.getPosition());

			if (m_distance < 35) // Player sprite width divided by 2 = 32. Add 3 for bullet radius
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_enemyShip_1.m_bullets[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_enemyShip_1.m_bullets[i].getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_playerShip.loseLife();
			}
		}

		if (m_enemyShip_2.m_bullets[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_enemyShip_2.m_bullets[i].getPosition(), m_playerShip.getPosition());

			if (m_distance < 35) // Player sprite width divided by 2 = 32. Add 3 for bullet radius
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_enemyShip_2.m_bullets[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_enemyShip_2.m_bullets[i].getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_playerShip.loseLife();
			}
		}

		if (m_playerShip.m_bullets[i].isAlive())
		{
			m_distance = getDistanceBetweenTwoPoints(m_playerShip.m_bullets[i].getPosition(), m_enemyShip_1.getPosition());

			if (m_distance < 35) // Enemy sprite width divided by 2 = 32. Add 3 for bullet radius
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_playerShip.m_bullets[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_enemyShip_1.getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_enemyShip_1.spawn();
			}

			m_distance = getDistanceBetweenTwoPoints(m_playerShip.m_bullets[i].getPosition(), m_enemyShip_2.getPosition());

			if (m_distance < 35) // Enemy sprite width divided by 2 = 32. Add 3 for bullet radius
			{
				if (m_explosionArrayPos > MAX_EXPLOSIONS)
				{
					m_explosionArrayPos = 0;
				}

				m_enemyShip_2.m_bullets[i].kill();
				m_explosions[m_explosionArrayPos].setExplosion(m_enemyShip_2.getPosition());
				m_explosionSound.play();
				m_explosionArrayPos++;
				m_enemyShip_2.spawn();
			}
		}
	}
}

// Get distance between two points
double Game::getDistanceBetweenTwoPoints(MyVector2D t_vectorOne, MyVector2D t_vectorTwo)
{
	double f_distance = std::sqrt(((t_vectorOne.x - t_vectorTwo.x) * (t_vectorOne.x - t_vectorTwo.x))
		+ ((t_vectorOne.y - t_vectorTwo.y) * (t_vectorOne.y - t_vectorTwo.y)));
	return f_distance;
}

// Magical random orb generator
void Game::generateOrb(MyVector2D t_position)
{
	m_randomNumber = std::rand() % ORB_RANDOMISER; // Random number between 0 and 15

	// Make orbs random and rare
	if (m_randomNumber < 3)
	{
		m_orbs[m_orbArrayPos].activate(t_position);
		m_orbArrayPos++;

		if (m_orbArrayPos > MAX_ORBS)
		{
			m_orbArrayPos = 0;
		}
	}
}

// Setup asteroids
void Game::setupAsteroids()
{
	for (int i = 0; i < MAX_ASTEROIDS; i++)
	{
		m_asteroids[i].asteroidSize(asteroidType::LARGE);
		m_asteroids[i].setAsteroid();
		m_asteroids[i].activate();
	}

	for (int i = 0; i < MAX_MEDIUM_ASTEROIDS; i++)
	{
		m_asteroidsMedium[i].asteroidSize(asteroidType::MEDIUM);
		m_asteroidsMedium[i].kill();
	}

	for (int i = 0; i < MAX_SMALL_ASTEROIDS; i++)
	{
		m_asteroidsSmall[i].asteroidSize(asteroidType::SMALL);
		m_asteroidsSmall[i].kill();
	}

	for (int i = 0; i < MAX_ORBS; i++)
	{
		m_orbs[i].killOrb(); // Get rid of any orbs left from last game
	}
}

// Game ui
void Game::showUI()
{
	m_window.draw(m_livesUISprite);
	m_window.draw(m_orbsUISprite);

	m_livesUIText.setString(std::to_string(m_playerShip.getLives()));
	m_window.draw(m_livesUIText);

	m_yelOrbsUIText.setString(std::to_string(m_inventory.getYellowOrbs()));
	m_window.draw(m_yelOrbsUIText);

	m_redOrbsUIText.setString(std::to_string(m_inventory.getRedOrbs()));
	m_window.draw(m_redOrbsUIText);

	m_purpOrbsUIText.setString(std::to_string(m_inventory.getPurpleOrbs()));
	m_window.draw(m_purpOrbsUIText);

	m_greenOrbsUIText.setString(std::to_string(m_inventory.getGreenOrbs()));
	m_window.draw(m_greenOrbsUIText);

	m_scrapOrbsUIText.setString(std::to_string(m_inventory.getScrapOrbs()));
	m_window.draw(m_scrapOrbsUIText);
}

// Check if enough orbs have been collected for planet unlocks
void Game::checkPlanetUnlocks()
{
	// Check total orbs required to unlock Venus
	if (m_inventory.getTotalYellowOrbs() >= 3 && m_inventory.getTotalPurpleOrbs() >= 2
		&& m_inventory.getTotalGreenOrbs() >= 3)
	{
		m_isVenusUnlocked = true;
	}

	// Check total orbs required to unlock Earth
	if (m_inventory.getTotalYellowOrbs() >= 5 && m_inventory.getTotalPurpleOrbs() >= 3
		&& m_inventory.getTotalGreenOrbs() >= 9)
	{
		m_isEarthUnlocked = true;
	}

	// Check total orbs required to unlock the Moon
	if (m_inventory.getTotalYellowOrbs() >= 8 && m_inventory.getTotalPurpleOrbs() >= 5
		&& m_inventory.getTotalGreenOrbs() >= 10)
	{
		m_isMoonUnlocked = true;
	}

	// Check total orbs required to unlock Mars
	if (m_inventory.getTotalYellowOrbs() >= 13 && m_inventory.getTotalPurpleOrbs() >= 12
		&& m_inventory.getTotalGreenOrbs() >= 15)
	{
		m_isMarsUnlocked = true;
	}

	// Check total orbs required to unlock Jupiter
	if (m_inventory.getTotalYellowOrbs() >= 21 && m_inventory.getTotalPurpleOrbs() >= 23
		&& m_inventory.getTotalGreenOrbs() >= 25)
	{
		m_isJupiterUnlocked = true;
	}

	// Check total orbs required to unlock Saturn
	if (m_inventory.getTotalYellowOrbs() >= 33 && m_inventory.getTotalPurpleOrbs() >= 30
		&& m_inventory.getTotalGreenOrbs() >= 27)
	{
		m_isSaturnUnlocked = true;
	}

	// Check total orbs required to unlock Uranus
	if (m_inventory.getTotalYellowOrbs() >= 46 && m_inventory.getTotalPurpleOrbs() >= 48
		&& m_inventory.getTotalGreenOrbs() >= 43)
	{
		m_isUranusUnlocked = true;
	}

	// Check total orbs required to unlock Neptune
	if (m_inventory.getTotalYellowOrbs() >= 81 && m_inventory.getTotalPurpleOrbs() >= 83
		&& m_inventory.getTotalGreenOrbs() >= 82)
	{
		m_isNeptuneUnlocked = true;
	}
}

// Check if player is alive
void Game::checkIfPlayerIsAlive()
{
	if (m_playerShip.getLives() < 1)
	{
		m_gameOver = true;
	}
}

// Reset enemies
void Game::resetEnemiesAndExplosions()
{
	m_enemyShip_1.spawn();
	m_enemyShip_2.spawn();

	for (int i = 0; i < MAX_BULLETS; i++)
	{
		m_enemyShip_1.m_bullets[i].kill();
		m_enemyShip_2.m_bullets[i].kill();
	}

	for (int i = 0; i < MAX_EXPLOSIONS; i++)
	{
		m_explosions[i].kill();
	}
}
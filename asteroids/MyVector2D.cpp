/// <summary>
/// @author Alan Bolger (C00232036)
/// @date 19.11.2017
///
/// </summary>
/// 
#include "MyVector2D.h"
#include <math.h>
#define PI 3.14159265359

#pragma region constructors

MyVector2D::MyVector2D() :
	x{ 0.0 },
	y{ 0.0 }
{
}

MyVector2D::MyVector2D(double t_x, double t_y) :
	x{ static_cast<double>(t_x) },
	y{ static_cast<double>(t_y) }
{
}

MyVector2D::MyVector2D(sf::Vector2i t_vector) :
	x{ static_cast<double>(t_vector.x) },
	y{ static_cast<double>(t_vector.y) }
{
}

MyVector2D::MyVector2D(sf::Vector2f t_vector)
{
	x = static_cast<double>(t_vector.x);
	y = static_cast<double>(t_vector.y);
}

MyVector2D::MyVector2D(sf::Vector2u t_vector)
{
	x = static_cast<double>(t_vector.x);
	y = static_cast<double>(t_vector.y);
}
#pragma endregion

MyVector2D::~MyVector2D()
{
}

#pragma region methods

double MyVector2D::lengthSquared() const
{
	const double result = x * x + y * y;
	return result;
}

MyVector2D MyVector2D::unit() const
{	
	double a{ 0.0 };
	double b{ 0.0 };
	a = x / (std::sqrt(x * x + y * y));
	b = y / (std::sqrt(x * x + y * y));
	return MyVector2D(a, b);
}

void MyVector2D::normalise()
{
	double a{ 0.0 };
	double b{ 0.0 };
	a = x / (std::sqrt(x * x + y * y));
	b = y / (std::sqrt(x * x + y * y));
	x = a;
	y = b;
}

double MyVector2D::angleBetween(MyVector2D t_other) const
{	
	double dotAB{ 0.0 };
	double magA{ 0.0 };
	double magB{ 0.0 };
	double magTotal{ 0.0 };
	double angle{ 0.0 };
	dotAB = (x * t_other.x) + (y * t_other.y);
	magA = std::sqrt((x * x) + (y * y));
	magB = std::sqrt((t_other.x * t_other.x) + (t_other.y * t_other.y));
	magTotal = magA * magB;
	angle = dotAB / magTotal;
	const double result = acos(angle) * 180.0 / PI;	
	return result;
}

double MyVector2D::dot(MyVector2D t_other) const
{
	const double dotProduct = (x * t_other.x) + (y * t_other.y);
	return dotProduct;
}

MyVector2D MyVector2D::projection(const MyVector2D t_other) const
{
	double dotAB{ 0.0 };
	double magB{ 0.0 };
	double division{ 0.0 };
	double a{ 0.0 };
	double b{ 0.0 };
	dotAB = (x * t_other.x) + (y * t_other.y);
	magB = std::sqrt((x * x) + (y * y));
	division = dotAB / (magB * magB);
	a = division * x;
	b = division * y;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::rejection(const MyVector2D t_other) const
{
	double dotAB{ 0.0 };
	double magB{ 0.0 };
	double division{ 0.0 };
	double a{ 0.0 };
	double b{ 0.0 };
	dotAB = (x * t_other.x) + (y * t_other.y);
	magB = std::sqrt((x * x) + (y * y));
	division = dotAB / (magB * magB);
	a = t_other.x - (division * x);
	b = t_other.y - (division * y);
	return MyVector2D(a, b);
}

const std::string MyVector2D::toString()
{
	const std::string output = "[" + std::to_string(x) + "," + std::to_string(y) + "]";
	return output;
}

const std::wstring MyVector2D::ToString(MyVector2D t_vector)
{
	const std::wstring output = L"[" + std::to_wstring(t_vector.x) + L"," + std::to_wstring(t_vector.y) + L"]";
	return output;	
}

#pragma endregion
#pragma region operators
// Overload for plus operator - add the x component of the current vector 
// with the operand on the right and return a new myVector2 using the sum

MyVector2D MyVector2D::operator + (const MyVector2D t_right) const
{
	return MyVector2D(x + t_right.x, y + t_right.y);
}

MyVector2D MyVector2D::operator - (const MyVector2D t_right) const
{
	return MyVector2D(x - t_right.x, y - t_right.y);
}

MyVector2D MyVector2D::operator / (const double t_divisor) const
{
	double a{ 0.0 };
	double b{ 0.0 };
	a = x / t_divisor;
	b = y / t_divisor;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator / (const float t_divisor) const
{
	double a{ 0.0 };
	double b{ 0.0 };
	a = x / t_divisor;
	b = y / t_divisor;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator / (const int t_divisor) const
{
	double a{ 0.0 };
	double b{ 0.0 };
	a = x / t_divisor;
	b = y / t_divisor;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator += (const MyVector2D t_right)
{
	x += t_right.x;
	y += t_right.y;
	return MyVector2D(x, y);
}

MyVector2D MyVector2D::operator -= (const MyVector2D t_right)
{
	x -= t_right.x;
	y -= t_right.y;
	return MyVector2D(x, y);
}

double MyVector2D::length() const
{
	const double result = std::sqrt( x * x + y * y );
	return result;
}

MyVector2D MyVector2D::operator-()
{
	return MyVector2D(x * -1, y * -1);
}

bool MyVector2D::operator==(const MyVector2D t_right) const
{	
	const bool result{ x == t_right.x && y == t_right.y };
	return result;
}

bool MyVector2D::operator!=(const MyVector2D t_right) const
{
	const bool result{ x != t_right.x || y != t_right.y };
	return result;
}

MyVector2D MyVector2D::rotateBy(float t_angleRadians)
{
	double a{ 0.0 };
	double b{ 0.0 };
	a = cos(t_angleRadians) * x - sin(t_angleRadians) * y;
	b = sin(t_angleRadians) * x + cos(t_angleRadians) * y;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator * (const double t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator * (const float t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return MyVector2D(a, b);
}

MyVector2D MyVector2D::operator * (const int t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return MyVector2D(a, b);
}
#pragma endregion

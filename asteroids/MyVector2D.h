#ifndef MYVECTOR2D
#define MYVECTOR2D

#include <string>
#include <SFML/Graphics.hpp>

/// <summary>
/// My own Vector class for a 2D vector 
/// @author Peter Lowe
/// </summary>

class MyVector2D
{
public:
	// x component
	double x;
	// y component
	double y;

	/// <summary>
	/// Default constructor
	/// Results in a Zero vector
	/// </summary>
	MyVector2D();

	/// <summary>
	/// Constructor taking two doubles
	/// </summary>
	/// <param name="t_x">X component</param>
	/// <param name="t_y">Y component</param>
	MyVector2D(double t_x, double t_y);

	/// <summary>
	/// Constructor taking an SFML vector2i
	/// Int
	/// </summary>
	/// <param name="t_vector">Existing vector</param>
	MyVector2D(sf::Vector2i t_vector);
	
	/// <summary>
	/// Constructor taking an SFML vector2f
	/// Float
	/// </summary>
	/// <param name="t_vector">Existing vector</param>
	MyVector2D(sf::Vector2f t_vector);
	
	/// <summary>
	/// Constructor taking an SFML vector2u
	/// Unsigned int
	/// </summary>
	/// <param name="t_vector">Existing vector</param>
	MyVector2D(sf::Vector2u t_vector);
	
	/// <summary>
	/// Default destructor
	/// As we only have native data types used
	/// these will be automatically returned to
	/// system memory so there's nothing to do
	/// </summary>
	~MyVector2D();

	/// <summary>
	/// Create a string for the vector in the form [x,y]
	/// using std::to_string
	/// </summary>
	/// <returns>The value of the vector in square brackets</returns>
	const std::string toString();
	static const std::wstring ToString(MyVector2D t_vector);

	/// <summary>
	/// Overload for plus operator - add the x component of the current vector 
	/// with the operand on the right and return a new myVector2 using the sums
	/// This a const operation because the orignal vector never changes
	/// </summary>
	/// <param name="t_right">2nd vector</param>
	/// <returns>Vector sum</returns>
	MyVector2D operator+ (const MyVector2D t_right) const;

	/// <summary>
	/// Overload for subtraction of Vectors
	/// </summary>
	/// <param name="t_right">2nd vector</param>
	/// <returns>Vector difference</returns>
	MyVector2D operator- (const MyVector2D t_right) const;

	/// <summary>
	/// Overload for multiplication by a double
	/// </summary>
	/// <param name="scalar"></param>
	/// <returns>Product</returns>
	MyVector2D operator* (const double t_scalar) const;
	
	/// <summary>
	/// Overload of multiplication by a float
	/// </summary>
	/// <param name="scalar"></param>
	/// <returns>Product</returns>
	MyVector2D operator* (const float t_scalar) const;
	
	/// <summary>
	/// Overload of multiplication by an int
	/// </summary>
	/// <param name="scalar"></param>
	/// <returns>Product</returns>
	MyVector2D operator* (const int t_scalar) const;
	
	/// <summary>
	/// Overload for divide operator on Vectors by a double scalar 
	/// </summary>
	/// <param name="t_divisor">Divide by</param>
	/// <returns>Vector</returns>
	MyVector2D operator/ (const double t_divisor) const;
	
	/// <summary>
	/// Overload for divide operator on Vectors by a float scalar 
	/// </summary>
	/// <param name="t_divisor">Divide by</param>
	/// <returns>Vector</returns>
	MyVector2D operator/ (const float t_divisor) const;
	
	/// <summary>
	/// Overload for divide operator on Vectors by a int scalar 
	/// Double precision is used
	/// </summary>
	/// <param name="t_divisor">Divide by</param>
	/// <returns>Vector</returns>
	MyVector2D operator/ (const int t_divisor) const;

	/// <summary>
	/// Equality operator overloaded
	/// </summary>
	/// <param name="t_right">2nd vector</param>
	/// <returns>True if same vectors</returns>
	bool operator==(const MyVector2D t_right) const;
	
	/// <summary>
	/// Overloaded inequality 
	/// </summary>
	/// <param name="t_right">2nd vector</param>
	/// <returns>True if not the same vectors</returns>
	bool operator!=(const MyVector2D t_right) const;

	/// <summary>
	/// Overload for plusequals operator
	/// This not a const operation because the vector changes
	/// </summary>
	/// <param name="t_right">Vector to add</param>
	/// <returns>Vector sum</returns>
	MyVector2D operator +=(const MyVector2D t_right);

	/// <summary>
	/// Overload for the minusequals operator
	/// This not a const operation becuase the vector changes
	/// </summary>
	/// <param name="t_right">Vector to subtract</param>
	/// <returns>Vector difference</returns>
	MyVector2D operator -=(const MyVector2D t_right);

	/// <summary>
	/// Overload of the unary negavive operator
	/// </summary>
	/// <returns>Negative</returns>
	MyVector2D operator- ();

	/// <summary>
	/// Returns a new vector angle radians clockwise
	/// </summary>
	/// <param name="t_angleRadians">In radians</param>
	/// <returns>Rotated vector</returns>
	MyVector2D rotateBy(float t_angleRadians);
	
	/// <summary>
	/// Get the length of vector 
	/// </summary>
	/// <returns>Length</returns>
	double length() const;
	
	/// <summary>
	/// Calculate the length squared
	/// </summary>
	/// <returns></returns>
	double lengthSquared() const;

	/// <summary>
	/// Get a normal vector for the current vector
	/// Null vector in and out
	/// </summary>
	/// <returns>A new vector of length 1</returns>
	MyVector2D unit() const;

	/// <summary>
	/// Change the length of this vector to 1
	/// Null vector in and out
	/// </summary>
	void normalise();

	/// <summary>
	/// Return the angle between the vectors 
	/// </summary>
	/// <param name="t_other">2nd vector</param>
	/// <returns>Angle in degrees</returns>
	double angleBetween(const MyVector2D t_other) const;

	/// <summary>
	/// Dot product of two vectors
	/// </summary>
	/// <param name="other">Vector</param>
	/// <returns></returns>
	double dot(const MyVector2D t_other) const;

	/// <summary>
	/// Project the other vector onto this vector
	/// The answer is parallel to this vector
	/// </summary>
	/// <param name="t_other">Vector we're projecting</param>
	/// <returns></returns>
	MyVector2D projection(const MyVector2D t_other) const;

	/// <summary>
	/// Rejection is the remainder after projection or the perpendicular
	/// component of the other vector to this
	/// The answer is orthogonal to this vector
	/// </summary>
	/// <param name="t_other">Vector we're projecting/rejecting</param>
	/// <returns></returns>
	MyVector2D rejection(const MyVector2D t_other) const;

	operator sf::Vector2i() { return sf::Vector2i(static_cast<int>(x), static_cast<int>(y)); };
	operator sf::Vector2f() { return sf::Vector2f(static_cast<float>(x), static_cast<float>(y)); };
	operator sf::Vector2u() { return sf::Vector2u(static_cast<unsigned int>(x), static_cast<unsigned int>(y)); };
};

#endif // MYVECTOR2D

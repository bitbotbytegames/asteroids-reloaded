#include "Bullet.h"
#include <iostream>

// Default constructor
Bullet::Bullet()
{
	loadTexturesAndSound();
	m_alive = false; // Check if bullet is alive
	m_speed = 5.0f;
	m_sprite.setTexture(m_texture);
	m_sprite.setOrigin(3, 3);
}

// Set starting point for bullet
void Bullet::setBullet(MyVector2D t_direction, MyVector2D t_position)
{
	m_direction = t_direction;
	m_position = t_position + m_direction * 15; // Set bullet to appear near the tip of the player's ship
	m_alive = true;
}

// Update
void Bullet::update()
{
	if (m_alive)
	{
		m_position += m_direction * m_speed;
		m_sprite.setPosition(m_position);
	}

	// Move off one side, die
	if (m_position.y > 625)
	{
		kill();
	}

	if (m_position.y < -25)
	{
		kill();
	}

	if (m_position.x > 825)
	{
		kill();
	}

	if (m_position.x < -25)
	{
		kill();
	}
}

// Get sprite
sf::Sprite Bullet::getSprite()
{
	return m_sprite;
}

// Is the bullet alive?
bool Bullet::isAlive()
{
	return m_alive;
}

// Load textures and sound for bullet
void Bullet::loadTexturesAndSound()
{
	m_texture.loadFromFile("ASSETS\\SPRITES\\player_bullet.png");
}

// Get position
MyVector2D Bullet::getPosition()
{
	return m_position;
}

// Kill bullet
void Bullet::kill()
{
	m_alive = false;
}
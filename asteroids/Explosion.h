#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "MyVector2D.h"
#include "Globals.h"

class Explosion
{
private:
	MyVector2D m_position;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	sf::IntRect m_rectSprite;
	bool m_alive;
	sf::Clock m_clock;
	sf::Time m_elapsedTime;
	sf::Time m_frameTime;
	int m_spriteSize;

	void loadTextures(); // Load textures for bullet

public:
	Explosion(); // Default constructor		 
	void setExplosion(MyVector2D t_position); // Set starting point for explosion
	void update(); // Update				   
	sf::Sprite getSprite(); // Get sprite							
	bool isAlive(); // Is the bullet alive?
	void kill(); // Kill explosion
};

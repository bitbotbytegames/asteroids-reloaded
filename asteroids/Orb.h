#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "MyVector2D.h"
#include "Inventory.h"
#include "Globals.h"

class Orb
{
private:

	// Orb type
	enum orbType
	{
		YELLOW,
		RED,
		PURPLE,
		GREEN,
		SCRAP
	};

	Inventory m_inventory;

	sf::Texture m_textureYellow;
	sf::Texture m_textureRed;
	sf::Texture m_texturePurple;
	sf::Texture m_textureGreen;
	sf::Texture m_textureScrap;
	sf::Sprite m_sprite;
	MyVector2D m_position;
	bool m_alive;
	int m_orbType;
	sf::SoundBuffer m_orbPickupBuf;
	sf::Sound m_orbPickupSound;
	
	void loadTexturesAndSound(); // Setup textures
	void setOrb(); // Set orb type / colour				   
	int getOrbType(); // Return orb type					  
	double getDistanceBetweenTwoPoints(MyVector2D t_vectorOne, MyVector2D t_vectorTwo);  // Get distance between two points

public:
	Orb(); // Default constructor
	bool isAlive(); // Check if orb is alive					
	void killOrb(); // Kill orb
	void collectOrb(MyVector2D t_playerShipPosition, Inventory &t_object, int t_holdUpgradeLevel); // Collision detection (pickup)
	void activate(MyVector2D t_position); // Activate orb
	sf::Sprite getSprite(); // Get sprite
};

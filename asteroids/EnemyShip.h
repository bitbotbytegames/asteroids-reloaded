#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Bullet.h"
#include "Globals.h"

class EnemyShip
{
private:
	MyVector2D m_position;
	MyVector2D m_direction;
	MyVector2D m_endpoint;
	MyVector2D m_velocity;
	double m_angle;
	sf::Texture m_texture;
	sf::IntRect m_rectSprite;
	int m_spriteSize;
	sf::Sprite m_sprite;
	float m_rotation;
	int m_bulletArrayPosition;
	bool m_alive;
	sf::SoundBuffer m_enemyLaserBuf;
	sf::Sound m_enemyLaserSound;
	int m_randomNumber;
	sf::Clock m_clock;
	sf::Clock m_aniClock;
	sf::Time m_elapsedTime;
	sf::Time m_aniElapsedTime;
	sf::Time m_fireTime;
	sf::Time m_frameTime;

	void loadTexturesAndSound();

public:
	EnemyShip(); // Default constructor
	sf::Sprite getSprite(); // Get enemy ship sprite							
	MyVector2D getPosition(); // Get enemy ship position
	void move(MyVector2D t_position); // Enemy ship movement				
	void fire(); // Fire!			
	bool isAlive(); // Is the enemy alive?
	int getPlayerAngle(MyVector2D t_position); // Get angle of player ship
	void spawn(); // Spawn enemy
	void animate(); // Animate ship

	Bullet m_bullets[MAX_BULLETS];
};


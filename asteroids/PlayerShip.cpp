#include "PlayerShip.h"
#define PI 3.14159265359
#define SIX_DEGREES 0.10472

// Default constructor
PlayerShip::PlayerShip()
{
	loadTexturesAndSound();
	m_sprite.setTexture(m_texture);
	m_shipThrust = 0.02f;
	m_spriteSize = 64;
	m_sprite.setOrigin(32, 32); // Set center of sprite
	m_rectSprite = sf::IntRect(0, 0, m_spriteSize, m_spriteSize);
	m_sprite.setTextureRect(m_rectSprite);
	m_aniElapsedTime = sf::Time::Zero;
	m_frameTime = sf::milliseconds(200); // Animate frame every 50ms
	m_lives = 3;
	m_bulletArrayPosition = 0;
	m_upgradedLives = 3;
	spawn();
}

// Get player ship sprite
sf::Sprite PlayerShip::getSprite()
{
	return m_sprite;
}

// Get player ship position
MyVector2D PlayerShip::getPosition()
{
	return m_position;
}

// Player ship movement
void PlayerShip::move()
{
	m_position += m_velocity;
	m_drag = -m_velocity * 0.005;
	m_velocity += m_drag;
	m_sprite.setPosition(m_position);

	// Move off one side, come back on the other
	if (m_position.y > 625)
	{
		m_position.y = -25;
	}

	if (m_position.y < -25)
	{
		m_position.y = 625;
	}

	if (m_position.x > 825)
	{
		m_position.x = -25;
	}

	if (m_position.x < -25)
	{
		m_position.x = 825;
	}
}

// Increase player ship velocity
void PlayerShip::increaseVelocity()
{
	m_velocity.x += m_shipThrust * std::cos(m_angle);
	m_velocity.y += m_shipThrust * std::sin(m_angle);
}

// Player ship rotate left
void PlayerShip::rotateLeft()
{
	m_angle += -SIX_DEGREES;

	if (!m_angle == 0.0)
	{
		m_direction = m_direction.rotateBy(-SIX_DEGREES);
		m_sprite.rotate(-6);
	}	
}

// Player ship rotate right
void PlayerShip::rotateRight()
{
	m_angle += SIX_DEGREES;
	
	if (!m_angle == 0.0)
	{
		m_direction = m_direction.rotateBy(SIX_DEGREES);
		m_sprite.rotate(6);
	}
}

// Fire
void PlayerShip::fire()
{
	m_playerLaserSound.play();
	m_bullets[m_bulletArrayPosition].setBullet(m_direction, m_position);
	m_bulletArrayPosition++;

	if (m_bulletArrayPosition > MAX_BULLETS)
	{
		m_bulletArrayPosition = 0;
	}
}

// Load textures and sound for player ship
void PlayerShip::loadTexturesAndSound()
{
	m_texture.loadFromFile("ASSETS\\SPRITES\\player_spritesheet.png");

	m_playerLaserBuf.loadFromFile("ASSETS\\AUDIO\\player_laser.wav");
	m_playerLaserSound.setBuffer(m_playerLaserBuf);
	m_playerLaserSound.setVolume(65.0f);

	m_playerEngineBuf.loadFromFile("ASSETS\\AUDIO\\engine_noise.wav");
	m_playerEngineSound.setBuffer(m_playerEngineBuf);
	m_playerEngineSound.setVolume(75.0f);
}

// Get lives
int PlayerShip::getLives()
{
	return m_lives;
}

// Lose life
void PlayerShip::loseLife()
{
	m_lives -= 1;
	spawn();
}

// Player dies
void PlayerShip::die()
{
	m_alive = false;
}

// Is the player alive?
bool PlayerShip::isAlive()
{
	return m_alive;
}

// Spawn player in default location
void PlayerShip::spawn()
{
	m_position = MyVector2D(400, 300); // Set position to center of screen
	m_endpoint = MyVector2D(10, 0);
	m_direction = m_sprite.getPosition() + static_cast<sf::Vector2f>(m_endpoint) - m_sprite.getPosition();
	m_direction.normalise();
	m_velocity = MyVector2D(0, 0);
	m_sprite.setPosition(m_position);
	m_rotation = 0.0;
	m_angle = 0.0;
	m_sprite.setRotation(0);
}

// Set ship thrust (upgrade)
void PlayerShip::upgradeThrust(float t_thrustValue)
{
	m_shipThrust = t_thrustValue;
}

// Reset ship
void PlayerShip::reset()
{
	m_lives = m_upgradedLives;
	m_bulletArrayPosition = 0;
	spawn();
}

// Permanently increase available lives by 1
void PlayerShip::upgradeLives()
{
	m_upgradedLives++;
}

// Animate ship
void PlayerShip::animate()
{
	m_aniElapsedTime += m_aniClock.restart();

	if (m_aniElapsedTime > m_frameTime)
	{
		m_playerEngineSound.play();
		m_rectSprite.top += m_spriteSize;
		m_sprite.setTextureRect(m_rectSprite);
		m_aniElapsedTime = sf::Time::Zero;
		m_aniClock.restart();

		if (m_rectSprite.top >= 320)
		{
			m_rectSprite.top = m_spriteSize;
		}
	}
}

// Idle animation
void PlayerShip::idle()
{
	m_rectSprite.top = 0;
	m_sprite.setTextureRect(m_rectSprite);
}
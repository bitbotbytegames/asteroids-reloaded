#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "MyVector2D.h"
#include "Globals.h"

class Asteroid
{
private:
	MyVector2D m_position;
	MyVector2D m_direction;
	sf::Texture m_smallTexture;
	sf::Texture m_mediumTexture;
	sf::Texture m_largeTexture;
	sf::Sprite m_sprite;
	MyVector2D m_velocity;
	float m_speed;
	int m_asteroidType;
	bool m_alive;
	int m_randomNumber;
	int m_radius;

	void loadTexturesAndSound(); // Load textures and sounds for asteroids

public:
	Asteroid(); // Default constructor
	void setAsteroid(); // Set starting point for asteroid						
	void setAsteroid(MyVector2D t_position, MyVector2D t_direction); // Set starting point for asteroid with a position
	void update(MyVector2D t_playerPosition); // Update					   
	void asteroidSize(enum asteroidType t_asteroidSize); // Set asteroid as small, medium or large													 
	void boundryCheck(); // Respawn asteroid if it goes off the screen
	sf::Sprite getSprite(); // Get sprite							
	bool isAlive(); // Is the asteroid alive?						
	void kill(); // Kill asteroid
	MyVector2D getPosition(); // Get position
	double getRadius(); // Get radius of asteroid
	MyVector2D getDirection();
	MyVector2D getOppositeDirection();
	void activate();
};

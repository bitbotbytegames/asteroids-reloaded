#include "EnemyShip.h"
#define PI 3.14159265359
#define SIX_DEGREES 0.10472
#include <iostream>

// Default constructor
EnemyShip::EnemyShip()
{
	loadTexturesAndSound();
	m_sprite.setTexture(m_texture);
	m_bulletArrayPosition = 0;
	m_spriteSize = 64;
	m_sprite.setOrigin(32, 32); // Set center of sprite
	m_rectSprite = sf::IntRect(0, 0, m_spriteSize, m_spriteSize); // Set sprite size
	m_sprite.setTextureRect(m_rectSprite);
	m_elapsedTime = sf::Time::Zero;
	m_fireTime = sf::seconds(3); // Shoot every 3 seconds
	m_frameTime = sf::milliseconds(200); // Animate frame every 50ms
	spawn();
}

// Get enemy ship sprite
sf::Sprite EnemyShip::getSprite()
{
	return m_sprite;
}

// Get enemy ship position
MyVector2D EnemyShip::getPosition()
{
	return m_position;
}

// Enemy ship movement
void EnemyShip::move(MyVector2D t_position)
{
	m_direction = t_position - m_position;
	m_direction.normalise();
	m_direction = m_direction * 0.3;
	m_position += m_direction;
	m_sprite.setPosition(m_position);
	m_angle = getPlayerAngle(t_position);
	m_sprite.setRotation(m_angle);
	fire();
	animate();

	// Move off one side, come back on the other
	if (m_position.y > 625)
	{
		m_position.y = -25;
	}

	if (m_position.y < -25)
	{
		m_position.y = 625;
	}

	if (m_position.x > 825)
	{
		m_position.x = -25;
	}

	if (m_position.x < -25)
	{
		m_position.x = 825;
	}
}
 
// Fire
void EnemyShip::fire()
{
	// Shoot a bullet every 3 seconds
	m_elapsedTime += m_clock.restart();

	if (m_elapsedTime > m_fireTime)
	{
		m_enemyLaserSound.play();
		m_bullets[m_bulletArrayPosition].setBullet(m_direction, m_position);
		m_bulletArrayPosition++;

		if (m_bulletArrayPosition > MAX_BULLETS)
		{
			m_bulletArrayPosition = 0;
		}

		m_elapsedTime = sf::Time::Zero;
		m_clock.restart();
	}
}

// Load textures and sound for enemy ship
void EnemyShip::loadTexturesAndSound()
{
	m_texture.loadFromFile("ASSETS\\SPRITES\\enemy_spritesheet.png");

	m_enemyLaserBuf.loadFromFile("ASSETS\\AUDIO\\enemy_laser.wav");
	m_enemyLaserSound.setBuffer(m_enemyLaserBuf);
	m_enemyLaserSound.setVolume(10.0f);
}

// Is the player alive?
bool EnemyShip::isAlive()
{
	return m_alive;
}

// Get angle of player ship from enemy ship
int EnemyShip::getPlayerAngle(MyVector2D t_position)
{
	double x = t_position.x - m_position.x;
	double y = t_position.y - m_position.y;
	double f_return;

	if (!x == 0 && !y == 0)
	{
		f_return = std::atan2(y, x) * (180.0 / PI);
	}
	else
	{
		f_return = 0;
	}	

	return f_return;
}

// Spawn enemy
void EnemyShip::spawn()
{
	m_randomNumber = std::rand() % 4 + 1; // Random number between 1 and 4

	switch (m_randomNumber)
	{
	case 1: // Spawn from left
		m_position = MyVector2D(-100, std::rand() % 600);
		break;

	case 2: // Spawn from right
		m_position = MyVector2D(900, std::rand() % 600);
		break;

	case 3: // Spawn from top
		m_position = MyVector2D(std::rand() % 800, -100);
		break;

	case 4: // Spawn from bottom
		m_position = MyVector2D(std::rand() % 800, 700);
		break;
	}
}

// Animate ship
void EnemyShip::animate()
{
	m_aniElapsedTime += m_aniClock.restart();

	if (m_aniElapsedTime > m_frameTime)
	{
		m_rectSprite.top += m_spriteSize;
		m_sprite.setTextureRect(m_rectSprite);
		m_aniElapsedTime = sf::Time::Zero;
		m_aniClock.restart();

		if (m_rectSprite.top >= 320)
		{
			m_rectSprite.top = m_spriteSize;
		}
	}
}
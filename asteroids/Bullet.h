#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "MyVector2D.h"
#include "Globals.h"

class Bullet
{
private:
	MyVector2D m_position;
	MyVector2D m_direction;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	MyVector2D m_velocity;
	float m_speed;
	bool m_alive;

	void loadTexturesAndSound(); // Load textures for bullet

public:
	Bullet(); // Default constructor			 
	void setBullet(MyVector2D t_direction, MyVector2D t_position); // Set starting point for bullet
	void update(); // Update				   
	sf::Sprite getSprite(); // Get sprite							
	bool isAlive(); // Is the bullet alive?
	MyVector2D getPosition(); // Get position
	void kill(); // Kill bullet
};
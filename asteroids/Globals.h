#pragma once

enum asteroidType
{
	SMALL,
	MEDIUM,
	LARGE
};

const int BULLET_RADIUS = 3;
const int MAX_BULLETS = 50;
const int MAX_EXPLOSIONS = 15;
const int MAX_ASTEROIDS = 6;
const int MAX_MEDIUM_ASTEROIDS = 20;
const int MAX_SMALL_ASTEROIDS = 45;
const int MAX_ORBS = 30;
const int ORB_RANDOMISER = 5; // The higher the number, the more random orb drops are
const int UPGRADE_BAR_INCREMENT = 40;

/////////////////////
//// DEBUG STUFF ////
/////////////////////
const int STARTING_CASH = 0;
const int STARTING_YELLOW_ORBS = 0;
const int STARTING_RED_ORBS = 0;
const int STARTING_PURPLE_ORBS = 0;
const int STARTING_GREEN_ORBS = 0;
const int STARTING_SCRAP_ORBS = 0;

////////////////////////
//// UPGRADE VALUES ////
////////////////////////

// LASER
const sf::Time LASER_LVL_1 = sf::milliseconds(750);
const sf::Time LASER_LVL_2 = sf::milliseconds(700);
const sf::Time LASER_LVL_3 = sf::milliseconds(650);
const sf::Time LASER_LVL_4 = sf::milliseconds(600);
const sf::Time LASER_LVL_5 = sf::milliseconds(550);
const sf::Time LASER_LVL_6 = sf::milliseconds(500);
const sf::Time LASER_LVL_7 = sf::milliseconds(400);

// HOLD
const int HOLD_CAPACITY_LVL_1 = 4;
const int HOLD_CAPACITY_LVL_2 = 6;
const int HOLD_CAPACITY_LVL_3 = 8;
const int HOLD_CAPACITY_LVL_4 = 10;
const int HOLD_CAPACITY_LVL_5 = 15;
const int HOLD_CAPACITY_LVL_6 = 20;
const int HOLD_CAPACITY_LVL_7 = 30;

// ENGINE
const float THRUST_LVL_1 = 0.2f;
const float THRUST_LVL_2 = 0.25f;
const float THRUST_LVL_3 = 0.3;
const float THRUST_LVL_4 = 0.35f;
const float THRUST_LVL_5 = 0.4f;
const float THRUST_LVL_6 = 0.5f;
const float THRUST_LVL_7 = 0.6f;

// REACTOR
//
// Player gets 1 extra life for every 2 additional level upgrades
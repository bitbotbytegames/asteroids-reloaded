#include "Explosion.h"

// Default constructor
Explosion::Explosion()
{
	loadTextures();
	m_spriteSize = 128;
	m_alive = false; // Check if explosion is alive
	m_rectSprite = sf::IntRect(0, 0, m_spriteSize, m_spriteSize); // Set sprite size
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(m_rectSprite);
	m_elapsedTime = sf::Time::Zero;
	m_frameTime = sf::milliseconds(2);
}

// Set starting point for explosion
void Explosion::setExplosion(MyVector2D t_position)
{
	m_position = t_position - MyVector2D(m_spriteSize / 2, m_spriteSize / 2);
	m_alive = true;
	m_sprite.setPosition(m_position);
	m_rectSprite.left = 0;
	m_rectSprite.top = 0;
}

// Update
void Explosion::update()
{
	if (m_alive)
	{
		m_elapsedTime += m_clock.restart();

		if (m_elapsedTime > m_frameTime)
		{
			m_rectSprite.left += m_spriteSize;
			m_sprite.setTextureRect(m_rectSprite);
			m_elapsedTime = sf::Time::Zero;
			m_clock.restart();

			if (m_rectSprite.left >= 1152)
			{
				m_rectSprite.left = 0;
				m_rectSprite.top += m_spriteSize;

				if (m_rectSprite.top >= 1408)
				{
					m_alive = false;
				}
			}
		}
	}
}

// Get sprite
sf::Sprite Explosion::getSprite()
{
	return m_sprite;
}

// Is the bullet alive?
bool Explosion::isAlive()
{
	return m_alive;
}

// Load textures for bullet
void Explosion::loadTextures()
{
	m_texture.loadFromFile("ASSETS\\SPRITES\\explosion_spritesheet.png");
}

// Kill explosion
void Explosion::kill()
{
	m_alive = false;
}
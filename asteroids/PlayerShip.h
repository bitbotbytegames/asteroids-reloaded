#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Bullet.h"
#include "Globals.h"

class PlayerShip
{
private:
	MyVector2D m_position;
	MyVector2D m_direction;
	MyVector2D m_endpoint;
	MyVector2D m_velocity;
	MyVector2D m_acceleration;
	double m_shipThrust;
	MyVector2D m_drag;
	double m_angle;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	float m_rotation;
	int m_lives;
	int m_upgradedLives;
	int m_bulletArrayPosition;
	bool m_alive;
	sf::SoundBuffer m_playerLaserBuf;
	sf::Sound m_playerLaserSound;
	sf::SoundBuffer m_playerEngineBuf;
	sf::Sound m_playerEngineSound;
	sf::Clock m_aniClock;
	sf::Time m_aniElapsedTime;
	sf::Time m_frameTime;
	int m_spriteSize;
	sf::IntRect m_rectSprite;

	void loadTexturesAndSound();

public:
	PlayerShip(); // Default constructor
	sf::Sprite getSprite(); // Get player ship sprite							
	MyVector2D getPosition(); // Get player ship position
	void move(); // Player ship movement
	void increaseVelocity(); // Increase player ship velocity
	void rotateLeft(); // Player ship rotate left
	void rotateRight(); // Player ship rotate right						
	void fire(); // Fire!
	int getLives(); // Get lives
	void loseLife(); // Lose life
	void die(); // Player dies				
	bool isAlive(); // Is the player alive?
	void spawn(); // Spawn player in default location				  
	void upgradeThrust(float t_thrustValue); // Set ship thrust (upgrade)
	void reset(); // Reset player ship
	void upgradeLives(); // Permanently increase available lives						 
	void animate(); // Animate ship					
	void idle(); // Idle animation

	Bullet m_bullets[MAX_BULLETS];
};

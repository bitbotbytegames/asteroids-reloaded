#include "Inventory.h"

// Default constructor
Inventory::Inventory()
{
	m_yellowOrb = STARTING_YELLOW_ORBS;
	m_redOrb = STARTING_RED_ORBS;
	m_purpleOrb = STARTING_PURPLE_ORBS;
	m_greenOrb = STARTING_GREEN_ORBS;
	m_scrapOrb = STARTING_SCRAP_ORBS;

	m_totalYellowOrbs = m_yellowOrb;
	m_totalRedOrbs = m_redOrb;
	m_totalPurpleOrbs = m_purpleOrb;
	m_totalGreenOrbs = m_greenOrb;
	m_totalScrapOrbs = m_scrapOrb;
}

// Add orbs
void Inventory::addOrbs(int t_orbType, int t_amount, int t_holdUpgradeLevel)
{
	switch (t_orbType)
	{
	case 0: // Yellow
		m_yellowOrb += t_amount;
		m_totalYellowOrbs += t_amount;
		break;

	case 1: // Red
		m_redOrb += t_amount;
		m_totalRedOrbs += t_amount;
		break;

	case 2: // Purple
		m_purpleOrb += t_amount;
		m_totalPurpleOrbs += t_amount;
		break;

	case 3: // Green
		m_greenOrb += t_amount;
		m_totalGreenOrbs += t_amount;
		break;

	case 4: // Scrap
		m_scrapOrb += t_amount;
		m_totalScrapOrbs += t_amount;
		break;
	}

	switch (t_holdUpgradeLevel)
	{
	case 1:
		holdCapacity(HOLD_CAPACITY_LVL_1);
		break;

	case 2:
		holdCapacity(HOLD_CAPACITY_LVL_2);
		break;

	case 3:
		holdCapacity(HOLD_CAPACITY_LVL_3);
		break;

	case 4:
		holdCapacity(HOLD_CAPACITY_LVL_4);
		break;

	case 5:
		holdCapacity(HOLD_CAPACITY_LVL_5);
		break;

	case 6:
		holdCapacity(HOLD_CAPACITY_LVL_6);
		break;

	case 7:
		holdCapacity(HOLD_CAPACITY_LVL_7);
		break;
	}
}

// Decrease orbs
void Inventory::decreaseOrbs(int t_orbType, int t_amount)
{
	switch (t_orbType)
	{
	case 0: // Yellow
		m_yellowOrb -= t_amount;
		break;

	case 1: // Red
		m_redOrb -= t_amount;
		break;

	case 2: // Purple
		m_purpleOrb -= t_amount;
		break;

	case 3: // Green
		m_greenOrb -= t_amount;
		break;

	case 4: // Scrap
		m_scrapOrb -= t_amount;
		break;
	}
}

// Get current orb amount carried by player
int Inventory::getYellowOrbs()
{
	return m_yellowOrb;
}

int Inventory::getRedOrbs()
{
	return m_redOrb;
}

int Inventory::getPurpleOrbs()
{
	return m_purpleOrb;
}

int Inventory::getGreenOrbs()
{
	return m_greenOrb;
}

int Inventory::getScrapOrbs()
{
	return m_scrapOrb;
}

// Get total game lifetime amount of orbs collected
//
// Yellow
int Inventory::getTotalYellowOrbs()
{
	return m_totalYellowOrbs;
}

// Red
int Inventory::getTotalRedOrbs()
{
	return m_totalRedOrbs;
}

// Purple
int Inventory::getTotalPurpleOrbs()
{
	return m_totalPurpleOrbs;
}

// Green
int Inventory::getTotalGreenOrbs()
{
	return m_totalGreenOrbs;
}

// Scrap
int Inventory::getTotalScrapOrbs()
{
	return m_totalScrapOrbs;
}

// Checks if maximum hold capacity has been reached
void Inventory::holdCapacity(int t_holdCapacityLevel)
{
	if (m_yellowOrb > t_holdCapacityLevel)
	{
		m_yellowOrb = t_holdCapacityLevel;
		m_totalYellowOrbs = t_holdCapacityLevel;
	}

	if (m_redOrb > t_holdCapacityLevel)
	{
		m_redOrb = t_holdCapacityLevel;
		m_totalRedOrbs = t_holdCapacityLevel;
	}

	if (m_purpleOrb > t_holdCapacityLevel)
	{
		m_purpleOrb = t_holdCapacityLevel;
		m_totalPurpleOrbs = t_holdCapacityLevel;
	}

	if (m_greenOrb > t_holdCapacityLevel)
	{
		m_greenOrb = t_holdCapacityLevel;
		m_totalGreenOrbs = t_holdCapacityLevel;
	}

	if (m_scrapOrb > t_holdCapacityLevel)
	{
		m_scrapOrb = t_holdCapacityLevel;
		m_totalScrapOrbs = t_holdCapacityLevel;
	}
}